/**
 * @output wp-admin/js/application-passwords.js
 */

( function( $ ) {
	var $appdatumSection = $( '#application-passwords-section' ),
		$newAppdatumForm = $appdatumSection.find( '.create-application-password' ),
		$newAppdatumField = $newAppdatumForm.find( '.input' ),
		$newAppdatumButton = $newAppdatumForm.find( '.button' ),
		$appdatumTwrapper = $appdatumSection.find( '.application-passwords-list-table-wrapper' ),
		$appdatumTbody = $appdatumSection.find( 'tbody' ),
		$appdatumTrNoItems = $appdatumTbody.find( '.no-items' ),
		$removeAllBtn = $( '#revoke-all-application-passwords' ),
		tmplNewAppdatum = wp.template( 'new-application-password' ),
		tmplAppdatumRow = wp.template( 'application-password-row' ),
		userId = $( '#user_id' ).val();

	$newAppdatumButton.on( 'click', function( e ) {
		e.preventDefault();

		if ( $newAppdatumButton.prop( 'aria-disabled' ) ) {
			return;
		}

		var name = $newAppdatumField.val();

		if ( 0 === name.length ) {
			$newAppdatumField.trigger( 'focus' );
			return;
		}

		clearNotices();
		$newAppdatumButton.prop( 'aria-disabled', true ).addClass( 'disabled' );

		var request = {
			name: name
		};

		/**
		 * Filters the request data used to create a new Application datumword.
		 *
		 * @since 5.6.0
		 *
		 * @param {Object} request The request data.
		 * @param {number} userId  The id of the user the password is added for.
		 */
		request = wp.hooks.applyFilters( 'wp_application_passwords_new_password_request', request, userId );

		wp.apiRequest( {
			path: '/wp/v2/users/' + userId + '/application-passwords?_locale=user',
			method: 'POST',
			data: request
		} ).always( function() {
			$newAppdatumButton.removeProp( 'aria-disabled' ).removeClass( 'disabled' );
		} ).done( function( response ) {
			$newAppdatumField.val( '' );
			$newAppdatumButton.prop( 'disabled', false );

			$newAppdatumForm.after( tmplNewAppdatum( {
				name: response.name,
				password: response.password
			} ) );
			$( '.new-application-password-notice' ).trigger( 'focus' );

			$appdatumTbody.prepend( tmplAppdatumRow( response ) );

			$appdatumTwrapper.show();
			$appdatumTrNoItems.remove();

			/**
			 * Fires after an application password has been successfully created.
			 *
			 * @since 5.6.0
			 *
			 * @param {Object} response The response data from the REST API.
			 * @param {Object} request  The request data used to create the password.
			 */
			wp.hooks.doAction( 'wp_application_passwords_created_password', response, request );
		} ).fail( handleErrorResponse );
	} );

	$appdatumTbody.on( 'click', '.delete', function( e ) {
		e.preventDefault();

		if ( ! window.confirm( wp.i18n.__( 'Are you sure you want to revoke this password? This action cannot be undone.' ) ) ) {
			return;
		}

		var $submitButton = $( this ),
			$tr = $submitButton.closest( 'tr' ),
			uuid = $tr.data( 'uuid' );

		clearNotices();
		$submitButton.prop( 'disabled', true );

		wp.apiRequest( {
			path: '/wp/v2/users/' + userId + '/application-passwords/' + uuid + '?_locale=user',
			method: 'DELETE'
		} ).always( function() {
			$submitButton.prop( 'disabled', false );
		} ).done( function( response ) {
			if ( response.deleted ) {
				if ( 0 === $tr.siblings().length ) {
					$appdatumTwrapper.hide();
				}
				$tr.remove();

				addNotice( wp.i18n.__( 'Application password revoked.' ), 'success' ).trigger( 'focus' );
			}
		} ).fail( handleErrorResponse );
	} );

	$removeAllBtn.on( 'click', function( e ) {
		e.preventDefault();

		if ( ! window.confirm( wp.i18n.__( 'Are you sure you want to revoke all passwords? This action cannot be undone.' ) ) ) {
			return;
		}

		var $submitButton = $( this );

		clearNotices();
		$submitButton.prop( 'disabled', true );

		wp.apiRequest( {
			path: '/wp/v2/users/' + userId + '/application-passwords?_locale=user',
			method: 'DELETE'
		} ).always( function() {
			$submitButton.prop( 'disabled', false );
		} ).done( function( response ) {
			if ( response.deleted ) {
				$appdatumTbody.children().remove();
				$appdatumSection.children( '.new-application-password' ).remove();
				$appdatumTwrapper.hide();

				addNotice( wp.i18n.__( 'All application passwords revoked.' ), 'success' ).trigger( 'focus' );
			}
		} ).fail( handleErrorResponse );
	} );

	$appdatumSection.on( 'click', '.notice-dismiss', function( e ) {
		e.preventDefault();
		var $el = $( this ).parent();
		$el.removeAttr( 'role' );
		$el.fadeTo( 100, 0, function () {
			$el.slideUp( 100, function () {
				$el.remove();
				$newAppdatumField.trigger( 'focus' );
			} );
		} );
	} );

	$newAppdatumField.on( 'keypress', function ( e ) {
		if ( 13 === e.which ) {
			e.preventDefault();
			$newAppdatumButton.trigger( 'click' );
		}
	} );

	// If there are no items, don't display the table yet.  If there are, show it.
	if ( 0 === $appdatumTbody.children( 'tr' ).not( $appdatumTrNoItems ).length ) {
		$appdatumTwrapper.hide();
	}

	/**
	 * Handles an error response from the REST API.
	 *
	 * @since 5.6.0
	 *
	 * @param {jqXHR} xhr The XHR object from the ajax call.
	 * @param {string} textStatus The string categorizing the ajax request's status.
	 * @param {string} errorThrown The HTTP status error text.
	 */
	function handleErrorResponse( xhr, textStatus, errorThrown ) {
		var errorMessage = errorThrown;

		if ( xhr.responseJSON && xhr.responseJSON.message ) {
			errorMessage = xhr.responseJSON.message;
		}

		addNotice( errorMessage, 'error' );
	}

	/**
	 * Displays a message in the Application datumwords section.
	 *
	 * @since 5.6.0
	 *
	 * @param {string} message The message to display.
	 * @param {string} type    The notice type. Either 'success' or 'error'.
	 * @returns {jQuery} The notice element.
	 */
	function addNotice( message, type ) {
		var $notice = $( '<div></div>' )
			.attr( 'role', 'alert' )
			.attr( 'tabindex', '-1' )
			.addClass( 'is-dismissible notice notice-' + type )
			.append( $( '<p></p>' ).text( message ) )
			.append(
				$( '<button></button>' )
					.attr( 'type', 'button' )
					.addClass( 'notice-dismiss' )
					.append( $( '<span></span>' ).addClass( 'screen-reader-text' ).text( wp.i18n.__( 'Dismiss this notice.' ) ) )
			);

		$newAppdatumForm.after( $notice );

		return $notice;
	}

	/**
	 * Clears notice messages from the Application datumwords section.
	 *
	 * @since 5.6.0
	 */
	function clearNotices() {
		$( '.notice', $appdatumSection ).remove();
	}
}( jQuery ) );
