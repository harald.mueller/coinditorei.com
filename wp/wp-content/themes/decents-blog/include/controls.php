<?php
/**
 * excerpt lenth.
 */
if (!class_exists('WP_Customize_Control'))
{
    return null;
}

if (!function_exists('decents_blog_custom_excerpt_length')):
    function decents_blog_custom_excerpt_length($length)
    {
        if (is_admin())
        {
            return $length;
        }
        $ext_length = get_theme_mod('decents_blog_excerpt_length', '55', 'decents-blog');
        if (!empty($ext_length))
        {
            return $ext_length;
        }
        return 50;
    }
endif;
add_filter('excerpt_length', 'decents_blog_custom_excerpt_length', 999);

function decents_blog_sanitize_select($input, $setting)
{
    // Ensure input is a slug.
    $input = sanitize_key($input);

    // Get list of choices from the control associated with the setting.
    $choices = $setting
        ->manager
        ->get_control($setting->id)->choices;

    // If the input is a valid key, return it; otherwise, return the default.
    return (array_key_exists($input, $choices) ? $input : $setting->default);
}

function decents_blog_sanitize_checkbox($checked)
{
    return ((isset($checked) && true === $checked) ? true : false);
}