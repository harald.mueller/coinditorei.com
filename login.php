<?php 
session_start();
$email = "";
$isOK = "";
if (isset($_REQUEST["btn_ok"])) {
    include_once 'funktionen/datahandling.php';
    $user = getEntry(htmlentities($_REQUEST["email"]), "users");
    if (password_verify(htmlentities($_REQUEST["password"]), $user["udatumword"])) {
        //echo 'Valides datumwort!';
        $_SESSION["logged_in"] = "logged_in";
        $_SESSION["uKurzzeichen"] = $user["uKurzzeichen"];
        $_SESSION["uAnzeigename"] = $user["uAnzeigename"];
        $_SESSION["uEmail"] = $user["uEmail"];
        header("Location: index.php");
    } else {
        $isOK = "NOK";
        if (isset($_SESSION["logged_in"])) {
            unset($_SESSION["logged_in"]);
        }
        if (isset($_SESSION["uKurzzeichen"])) {
            unset($_SESSION["uKurzzeichen"]);
        }
        if (isset($_SESSION["uAnzeigename"])) {
            unset($_SESSION["uAnzeigename"]);
        }
        if (isset($_SESSION["uEmail"])) {
            unset($_SESSION["uEmail"]);
        }
    }
}
include 'inc.header.php';

?>
<form action="" method="post">
<section class="banner-home">
    <div class="container" style="margin-top: 40px;">
       <!--Row Start-->
        <div class="row">
            <div class="col-sm-6">
                <h1 data-aos="fade-left" data-aos-delay="300" style="margin: 50px 0;">Login</h1>
                <?php 
                if ($isOK == "NOK") {
                    echo '
                        <div class="alert alert-danger" role="alert">
                          Email unbekannt oder datumwort stimmt nicht.
                        </div>
                    ';
                }
                
                ?>
                <div class="input-group input-group" style="padding: 20px 20px 20px 0;">
                  <div class="input-group-prepend mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-lg" style="width: 100px;">Email:</span>
            		<input type="email" id="email" name="email" style="width: 250px;" class="form-control" aria-label="Large">
                  </div>
                  <div class="input-group-prepend mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-lg" style="width: 100px;">datumwort:</span>
            		<input type="password" id="password" name="password" style="width: 250px;" class="form-control" aria-label="Large">
                  </div>
                  <div class="input-group mb-3" style="margin: 20px 0 50px 0;">
        			<button name="btn_ok" class="btn btn-primary btn-lg" style="width: 350px;">OK</button>
                  </div>
                </div>
                    
    		</div>
        </div>
	</div>
</section>
</form>

<script type="text/javascript">
	document.getElementById("email").focus();
</script>
<?php include 'inc.footer.php'; ?>