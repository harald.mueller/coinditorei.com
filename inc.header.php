<?php @session_start();
include_once 'funktionen/_globals.php'; 
include_once 'funktionen/datahandling.php'; 
?>
<!doctype html>
<html lang="de">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Der Info-Shop über Kryptowährungen. Wir schauen den Coins ins Innere. Auf und in die Tortenböden, Füllungen, Crèmen, Couverturen usw.">
    <meta name="author" content="Bernhard Melmer, Harald G. Müller">

    <!-- Bootstrap CSS -->
<!-- 	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> 
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700"> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/main.css">
    <?php 
    if (substr($_SERVER['SCRIPT_FILENAME'], -9) != "index.php") {
      echo "<style>.banner-home {height: auto; padding-top: 140px;}</style>";
    }  
    ?>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <title>Coinditorei</title>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  </head>
  <body>

  

<!-- Header section Start-->
<header class="top">

  <div id="mySidenav" class="sidenav">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <a href="index.php"   <?php if (substr($_SERVER['SCRIPT_FILENAME'], -9)  == 'index.php')   { echo " class='active' "; } ?> >Coin-Home</a>
    <a href="index.php#jumpBlogs" >Blog</a>
    <a href="vitrine" >Vitrine</a>    
<!-- <a href="blog.php"    <?php if (substr($_SERVER['SCRIPT_FILENAME'], -8)  == 'blog.php')    { echo " class='active' "; } ?> >Blog</a>  -->
<!-- <a href="about.php"   <?php if (substr($_SERVER['SCRIPT_FILENAME'], -9)  == 'about.php')   { echo " class='active' "; } ?> >Coin-About</a>  -->
    <a href="rezepte.php" <?php if (substr($_SERVER['SCRIPT_FILENAME'], -11) == 'rezetpe.php') { echo " class='active' "; } ?> >Rezepte</a>
    <a href="haeppchen.php" <?php if (substr($_SERVER['SCRIPT_FILENAME'], -13) == 'haeppchen.php') { echo " class='active' "; } ?> >Häppchen</a>
    <a href="#" <?php if (substr($_SERVER['SCRIPT_FILENAME'], -13) == 'haeppchen.php') { echo " class='active' "; } ?> >Mitgliederbereich</a>
    <a href="#" <?php if (substr($_SERVER['SCRIPT_FILENAME'], -13) == 'haeppchen.php') { echo " class='active' "; } ?> >Kurse</a>
	<hr style="background-color: green;">
    <?php if (isset($_SESSION["logged_in"])) { ?>
    <a href="logout.php" <?php if (substr($_SERVER['SCRIPT_FILENAME'], -10) == 'logout.php') { echo " class='active' "; } ?> >Logout</a>
    <?php } else { ?>
    <a href="registrierung.php" <?php if (substr($_SERVER['SCRIPT_FILENAME'], -17) == 'registrierung.php') { echo " class='active' "; } ?> >Registrierung</a>
    <a href="login.php" <?php if (substr($_SERVER['SCRIPT_FILENAME'], -9) == 'login.php') { echo " class='active' "; } ?> >Login</a>
	<hr style="background-color: green;">
    <a href="impressum.php"   <?php if (substr($_SERVER['SCRIPT_FILENAME'], -13)  == 'impressum.php')   { echo " class='active' "; } ?> >Impressum</a>
    <?php }  ?>
  </div>

  <nav id="navbar">
   <!-- container Start-->
    <div class="container">
       
       <!--Row Start-->
       <div class="row">
        <div class="col-8">
            <a href="index.php"><img src="images/logo250g.png" class="logo" alt="logo"></a>
        </div>
         <div class="col-4">
            <div class="social-icons square">
              <div id="page-content-wrapper"> 
                  <span class="slide-menu" onclick="openNav()"><i class="fa fa-bars" aria-hidden="true"></i></span>
              </div>
            </div>
         </div>
       </div>
       <!--Row Ended-->
              
    </div>
   <!-- container Ended-->
  </nav>

</header>
<!-- Header section Ended-->
<?php 
if (isset($_SESSION["logged_in"])) {
    echo '<div style="background-color: green; color: white; text-align: center;">eingeloggt: ['.$_SESSION["uKurzzeichen"].']</div>';
}
?>
