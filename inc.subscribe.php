<section class="Subscribe">
    <div class="container">
      <!-- Grid row-->
        <div class="row">
        <!-- Grid column -->
        <div class="col-lg-6 col-sm-6 col1">
           <div class="heading" data-aos="fade-right" data-aos-delay="300">
              <h3>Weitere Törtchen oder Pralinen?</h3>
              <h6>Schreiben Sie sich für den Newsletter ein</h6>
           </div>
        </div>
        <div class="col-lg-6 col-sm-6 col1">
           <form>
              <div class="input-group" data-aos="fade-left" data-aos-duration="800">
                 <input name="email" id="email" type="email" disabled="disabled" placeholder="Ihre Emailadresse" required>
                 <button class="btn btn-info" type="submit" disabled="disabled">Einschreiben</button>
              </div>
           </form>
         </div>
        </div>
    </div>
</section>