<!-- Footer -->
<footer class="page-footer font-small indigo">
<div class="gradient"></div>
    <!-- Footer Links -->
    <div class="container">

      <!-- Grid row-->
      <div class="row text-center d-flex justify-content-center">

        <!-- Grid column -->
        <div class="col-md-12">
            <a href="index.php"><img src="images/logo250inverse-g.png" alt="footer-logo"></a>
        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row-->

      <!-- Grid row-->
      <div class="row d-flex text-center justify-content-center mb-md-0 pb-4">

        <!-- Grid column -->
        <div class="col-md-8 col-12">
          <address>
              <p>
              <span>Der Info-Shop über Kryptowährungen.</span>
              <span>Wir schauen den Coins ins Innere. Auf und in die Tortenböden, Füllungen, Crèmen, Couverturen usw.</span>
<!--               <span class="call">Buchungen : +41 79 700 14 14</span> -->
              <br>
              <a href="impressum.php">Impressum</a>
              </p>
          </address>
        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row-->

      <!-- Grid row-->
      <div class="row pb-3">

<?php //include 'inc.footer-social-media.php' ?>        
        <!-- Grid column -->
        <div class="col-sm-12 copy-right">
            <p>© 2021-<?= date("Y") ?> , All Rights Reserved.</p>
        </div>
      </div>
      <!-- Grid row-->

    </div>
    <!-- Footer Links -->

  </footer>
  <!-- Footer -->
   
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <!-- Custom JavaScript -->
    <script src="js/animate.js"></script>
    <script src="js/custom.js"></script>

  </body>
</html>
