<?php
function getDateiname($auswahl) {
    $ret = "";
    switch (strtolower($auswahl)) {
        case "blogs":
            $ret = "./data/blogs.json";
            break;        
        case "users":
            $ret = "./data/users.json";
            break;        
        default:
            $ret = "";
            break;
    }
    return $ret;
}

function getId() {
    $date = date_create();
    return date_timestamp_get($date);
}

function getRow($rowID, $Typ) {
    $returnArray = array();
    $entries = getEntry($rowID, $Typ);
    if (array_key_exists($rowID , $entries)) {
        $returnArray = $entries[$rowID];
    }
    return $returnArray;
}

function getEntry($entry, $Typ) {
    $arr = getJsonData($Typ);
    if (count($arr) < 1) {
        echo 'Keine Daten vorhanden.';
    } else {
        //echo "<pre>".print_r($blogs,TRUE)."</pre>";
        foreach ($arr as $key => $value) {
            if ($entry == $key) {
                //echo "<pre>".print_r($value,TRUE)."</pre>";
                return $value;
            }
        }
    }
}

function getJsonData($Typ) {
    $blogs = array();
    $fileName = getDateiname($Typ);
    if (file_exists($fileName)) {
        $json_data = file_get_contents($fileName);
        $blogs = json_decode($json_data, true);
        return $blogs;
    } else {
        return "Error: Die Json-Datei fehlt!";
    }
}

function getBlogListe() {
    $out='';
    $blogs = getJsonData("blogs");
    krsort($blogs);
    if (count($blogs) < 1) {
        $out .= 'Keine Daten vorhanden.';
    } else {
        foreach ($blogs as $key => $value) {
            $bDate = $value["bDatErsterfassung"];
            if (strlen($value["bDatLastUpdate"]) > 5) {
                $bDate = '<span style="font-size: 5pt;">'.$value["bDatLastUpdate"].'</span> updated';
            }
            $link = '';
            if (strlen($value["bLink"])) {
                $link = '<span style="font-size: 7pt;"><a href="'.$value["bLink"].'" target="_blank">'.cropText($value["bLink"], 20).'...</a></span> <span style="font-size: 5pt;">('.$value["bLinkAbrufDat"].')</span>';
            }
            $out .= '
            <div class="col-md-6 col-sm-12" style="margin-bottom: 25px;">
                 <div class="row">
                     <div class="col-lg-3 col-sm-12">'; 
            
            if (isset($_SESSION["logged_in"])) {
                $key .= '&action=Update';
                $out .= '<a href="blog_single.php?key='.$key.'">Update</a>';
            } else {
                $key .= '&action=View';
            }
            
            $out .= '   <a href="blog_single.php?key='.$key.'" class="d-block mb-4 h-100">
                          <img class="img-fluid img-thumbnail" src="'.$value["bSymbolbild"].'" alt="" style="width: 100px; height: 100px;" >
                        </a>
                     </div>
                     <div class="col-lg-9 col-sm-12 inner-content">
                         <h5><a href="blog_single.php?key='.$key.'">'.$value["bTitel"].'</a></h5>
                         <p><span style="font-size: 7pt;" data-toggle="tooltip" data-placement="top" title="'.$value["bAutorNam"].'">'.$bDate.' ['.$value["bAutorKrz"].']</span></p>
                         <a href="blog_single.php?key='.$key.'"><h6>'.cropText($value["bTeaser"],100).'</h6></a>
                         <a href="blog_single.php?key='.$key.'"><p>'.cropText($value["bText"],100).'</p></a>
                         <p>'.$link.'<br></p>
                     </div>
                 </div> 
             </div>
             ';
        }
    }
    $out .= '';
    return $out;
}

function cropText($string, $laenge) {
    $out = $string;
    if (strlen($string) > $laenge) {
        $out = substr($string, 0, $laenge) . "...";
    }
    return $out;
}
/**
 * Insert and Update
 * @param Array $aDataSet
 * @param String $Key
 * @param String $Typ
 */
function saveAnEntry($aDataSet, $Key, $Typ) {
    $DataList = getJsonData($Typ);
    $DataList[$Key] = $aDataSet;
    saveList($DataList, $Typ);
    return TRUE;
}
function deleteRow($delID, $Typ) {
    $fileName = getDateiname($Typ);
    $entries = getBlogs($fileName);
    unset($entries[$delID]);
    saveList($entries, $Typ);
}

function saveList($DataList, $Typ) {
    $isOK = false;
    $fileName = getDateiname($Typ);
    $json_data = json_encode($DataList, JSON_PRETTY_PRINT);
    $isOK = file_put_contents($fileName, $json_data);
    return $isOK;
}
function checkText($uebergabeWort) {
    $umlaute = array("<",">","/","\\","&","ä", "ö", "ü", "Ä", "Ö", "Ü", "ß", "à","á","é","è","C","ç","ñ","Ñ","õ","ó","ò","ú");
    $ersatz = array(  "", "", "", "" ,"u","ae","oe","ue","Ae","Oe","Ue","ss","a","a","e","e","C","c","n","N","o","o","o","u");
    return str_replace ($umlaute,$ersatz,$uebergabeWort);
}

function getClearMobiltelString($subject) {
    $subject = strip_tags($subject);
    $subject = stripcslashes($subject);
    $search  = array("'", "\"", "(", ")", "-", ".", ",", ":", "_", " ");
    $replace = array("´", ""  , "" , "" , "" , "" , "" , "" , "" , "");
    return str_replace($search, $replace, $subject);
}

function getClearString($subject) {
    $subject = strip_tags($subject);
    $subject = stripcslashes($subject);
    $search  = array("'", "\"");
    $replace = array("´", "");
    return str_replace($search, $replace, $subject);
}
function zufallsstring ( $sting_laenge ) {
    srand ((double)microtime()*1000000);
    $zufall = rand();
    $zufallsstring = substr( md5($zufall) , 0 , $sting_laenge );
    return $zufallsstring;
}
?>