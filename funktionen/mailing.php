<?php
include_once '_globals.php';
$mailheader = array(
    'From' => 'Coinditorei <'.$APP_EMAIL.'>',
    'Reply-To' => '<'.$APP_EMAIL.'>',
    'X-Mailer' => 'PHP/' . phpversion()
);

$betreff = "";
$ScreenMsg = "";
if (isset($_REQUEST["btn_anmeldung"])) {
    if (isset($_REQUEST["email"])) {
        $email = strip_tags( $_REQUEST["email"] );
        $to = $email;
 
        $mobiltel = getClearMobiltelString( $_REQUEST["mobiltel"] );
        $vorname  = getClearString( $_REQUEST["vorname"] );
        $nachname = getClearString( $_REQUEST["nachname"] );
        
        // Mail it
        $mailOK = @mail($to, utf8_decode($betreff), utf8_decode($text), $mailheader);
        if ($mailOK) {
            $ScreenMsg = "Es wurde ein Mailgesendet an<br><b>".$to."</b>";
        }       
    }
}

if (isset($_REQUEST["btn_email_getcomment"])) {
    $maillink = '';
    if ($_SERVER["HTTP_HOST"] == "localhost") {
        $maillink .= 'http://localhost/coinditorei.com/';
    } else {
        $maillink .= 'https://haraldmueller.ch/coinditorei/';
    }
    $maillink .= 'blog_single.php?key='.$_REQUEST["blogKey"].'&action=View&email='.htmlentities($_REQUEST["email"]).'';

    
    $to = htmlentities($_REQUEST["email"]);
    $betreff = "Link zu einem Kommenar bei ".$APPNAME;
    $message = '
Liebe:r Besucher:in der '.$APPNAME.'

Sie haben sich angemneldet, einen Kommentar zum Beitrag

=============================================
  '.$_REQUEST["blogTitel"].'
=============================================

zu schreiben. Dafür danken wir Ihnen. Bitte achten Sie auf die 
"Nettikette" und bleiben Sie sachlich.

Benutzen Sie folgenden Link um zur Kommentar-Erfassung zu gelangen

'.$maillink.'

'.$MAILSIGNATUR;

    $mailOK = mail($to, utf8_decode($betreff), utf8_decode($message), $mailheader);
//echo '<pre>'.print_r($_REQUEST, TRUE).'</pre>';
$ScreenMsg = '
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> 

            <div class="alert alert-success" role="alert">
Vielen Dank.<br><br>Es ist eine Email nach <b>'.$to.'</b> unterwegs.<br>
Bitte öffnen Sie Ihr Mail und benutzen Sie den Link um einen Kommentar<br>
zum Thema "<b>'.$_REQUEST["blogTitel"].'</b>" zu schreiben<a href="'.$maillink.'">.</a>
<br><br>
<a href="../index.php#jumpBlogs" class="btn btn-success">zurück zur Blog-Liste</a>
            </div>
           ';
    if ($mailOK) {
        echo $ScreenMsg;
    }
    
}



?>