<!-- Blog section Start-->
<section class="blog" id="jumpBlogs" >
   <div class="gradient"></div>
    <!-- Page Content -->
    <div class="container">
         <div class="heading" data-aos="fade-up" data-aos-delay="300">
          <h2>Coinditoren-Blog</h2>
            <h3>Die inspirierenden Blogbeiträge</h3>
         	<?php  if (isset($_SESSION["logged_in"])) { ?>
         			<br><br>
         			<form action="blog_single.php" method="post">
         				<input type="hidden" name="action" value="NEW">
         				<input type="submit" name="submit" value="Neuer Beitrag" class="btn btn-primary">
                	</form>
         	<?php  } ?>
         </div>
         <div class="row" data-aos="fade-up" data-aos-delay="500">
            <?php  echo getBlogListe();  ?>
         </div>
<hr>
    </div>
    <!-- /.container -->
</section>
<!-- Blog section Ended-->