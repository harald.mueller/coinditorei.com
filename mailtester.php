<?php
$isOK = false;
$email = "";
if (isset($_REQUEST["email"])) {
    $email = htmlentities($_REQUEST["email"]);
    if (strlen($email) > 7 ) {
        //mailen
        $betreff = "Mailtest";
        $from  = "From: ".$_SERVER["SERVER_NAME"]." <automailer@".strtolower($_SERVER["SERVER_NAME"]).">\n";
        $from .= "Reply-To: harald.mueller@bluewin.ch\n";
        $from .= "Content-Type: text/html\n";
        $text  = "Herzlich willkommen\n<br>";
        $text .= print_r($_SERVER["SERVER_NAME"], TRUE);
        $text .= " (".print_r($_SERVER["SERVER_ADDR"], TRUE).")";
        $text .= "<br><br>Mit freundichen Gruessen";
        echo "mail to: ".$email;
        $isOK = mail($email, $betreff, $text, $from);
        $errMsg = error_get_last()['message'];
        if ($isOK) {
            echo "<strong style='color: green;'> OK</strong>"; 
        } else {
            echo "<pre style='color: red;'>NotOK";
            echo " -> Error Message: [".$errMsg."]";
            echo "</pre>";
        }
        echo "<br>".print_r($_SERVER["SERVER_NAME"], TRUE);
        echo " (".print_r($_SERVER["SERVER_ADDR"], TRUE).")";
    }
    
} else {
?>
<form action="" method="post" >
  <div class="container">
    <label for="email">E-Mailadresse:</label>
    <input type="email" id="email" name="email" value="<?= $email ?>" >
    <button type="submit" id="btn_OK" name="btn_OK" >OK</button>
  </div>
</form>
<?php 
}
?>