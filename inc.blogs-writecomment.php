<?php 
$fileName = "data/kommentare/comments-zu-".$_REQUEST["key"].".json";
$count = 0;
$allComments = array();
if (file_exists($fileName)) {
    $json_data = file_get_contents($fileName);
    $allComments = json_decode($json_data, true);
    $count = count($allComments);
}
?>
<hr>
<div class="comment-box" id="comment-box" >
        <div class="row">
           <div class="col-sm-12">
               <h3>Kommentare (<b><?= $count ?></b>)</h3>
         
           </div>
        </div>
        <div class="row">
           <div class="col-sm-12">
<?php  

if ($count > 0) {
    foreach ($allComments as $key => $value) {
        echo '<br>';
        echo '<p>'.$value["datumzeit"].'</p>';
        echo '<p><b>'.$value["anzeigename"].'</b></p>';
        echo '<p>'.$value["kommentar"].'</p>';
    }
}
?>			<br>	<br>	<br>
           </div>
        </div>

<?php 
if (!isset($_REQUEST["email"])) {
?>
         <div class="row" id="schreib1komment">
           <div class="col-sm-12">
               <button type="button" class="btn btn-primary" disabled>Schreib ein Kommentar</button>
           </div>
        </div>
        <form action="funktionen/mailing.php" method="post">
        	<input type="hidden" name="blogKey" value="<?= $_REQUEST["key"] ?>">
        	<input type="hidden" name="blogTitel" value="<?= $blogValue["bTitel"] ?>">
            <div class="row"  data-aos="fade-up" data-aos-duration="800">
              <div class="col-sm-12 col1">
              	<p>Es wird Ihnen ein Zugangslink für den Kommentar zugeschickt.</p>
              </div>
              <div class="col-sm-6 col1">
                <input type="email" name="email" class="form-control" placeholder="Email" required>
              </div>
              <div class="col-sm-6 col1">
                <button type="submit" name="btn_email_getcomment" class="btn btn-primary">Email mit Link anfordern</button>
              </div>
             </div>
        </form>

<?php 
} else {
    $email = $_REQUEST["email"];
?>
        <div class="row">
           <div class="col-sm-12">
               <button type="button" class="btn btn-primary" disabled>Bitte hier nun Ihr Kommentar</button>
           </div>
        </div>
        <form action="funktionen/comment.php" method="post">
        	<input type="hidden" name="blogKey" value="<?= $_REQUEST["key"] ?>">
        	<input type="hidden" name="email" value="<?= $email ?>">
        	<input type="hidden" name="requestURL" value="<?= $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>">
            <div class="row"  data-aos="fade-up" data-aos-duration="800">
              <div class="col-sm-6 col1">
                <input type="email" name="emailDisplay" class="form-control" placeholder="Email" disabled="disabled" value="<?= $email ?>">
              </div>
              <div class="col-sm-6 col1">
                <input type="text" name="anzeigename" class="form-control" placeholder="Anzeige-Name" required>
              </div>
              <div class="col-sm-12 col1">
                <textarea class="form-control" name="kommentar" placeholder="Mitteilung" rows="4" required></textarea>
              </div>
              <div class="col-sm-12 col1">
                <button type="submit" name="btn_kommentar_senden" class="btn btn-primary">Kommentar senden</button>
                <br><a href="index.php#jumpBlogs" class="btn btn-primary">zurück zur Blog-Liste</a>
              </div>
             </div>
        </form>
<?php 
}
?>

</div>
