<?php 
$isOK = "";
$status = "View";
$blogKey = "";
$bTyp = "";
$bTitel = "";
$bTeaser = "";
$bText = "";
$bTextIllu = "";
$bLink = "";
$bLinkAbrufDat = "";
$bDatErsterfassung = "";
$bDatLastUpdate = "";
$bSymbolbild = "";
$bAutorKrz = "";
$bAutorNam = "";

include 'inc.header.php';

if (isset($_SESSION["logged_in"])) {
    $bAutorKrz = $_SESSION["uKurzzeichen"];
    $bAutorNam = $_SESSION["uAnzeigename"];
}

if (isset($_REQUEST["btn_speichern"])) {
    //echo "<pre>REQUEST: ".print_r($_REQUEST, TRUE)."</pre>";
    $status = $_REQUEST["action"];
    $blogKey = $_REQUEST["bKey"];
    $bTyp = $_REQUEST["bTyp"];
    $bTitel = htmlentities($_REQUEST["bTitel"]);
    $bTeaser = htmlentities($_REQUEST["bTeaser"]);
    $bText = htmlentities($_REQUEST["bText"]);
    $bTextIllu = htmlentities($_REQUEST["bTextIllu"]);
    $bLink = htmlentities($_REQUEST["bLink"]);
    $bLinkAbrufDat = htmlentities($_REQUEST["bLinkAbrufDat"]);
    $bDatErsterfassung = $_REQUEST["bDatErsterfassung"];
    $bDatLastUpdate = $_REQUEST["bDatLastUpdate"];
    $bSymbolbild = $_REQUEST["bSymbolbild"];
    
    if (strlen(trim($blogKey)) < 10) {
        $blogKey = date("Ymd-His");
    }
    $newBlogArr = array();
    $newBlogArr["bTyp"] = $bTyp;
    $newBlogArr["bTitel"] = $bTitel;
    $newBlogArr["bTeaser"] = $bTeaser;
    $newBlogArr["bText"] = $bText;
    $newBlogArr["bTextIllu"] = $bTextIllu;
    $newBlogArr["bLink"] = $bLink;
    $newBlogArr["bLinkAbrufDat"] = $bLinkAbrufDat;
    $newBlogArr["bAutorKrz"] = $bAutorKrz;
    $newBlogArr["bAutorNam"] = $bAutorNam;
    $newBlogArr["bDatErsterfassung"] = $bDatErsterfassung;
    $newBlogArr["bDatLastUpdate"] = $bDatLastUpdate;
    $newBlogArr["bSymbolbild"] = $bSymbolbild;
    //$saveInfo = "<pre>newBlogArr[".$blogKey."]<br>".print_r($newBlogArr, TRUE)."</pre>";
    $isOK = saveAnEntry($newBlogArr, $blogKey, "blogs");
    //echo $saveInfo;
} else {

    if (isset($_REQUEST["action"]) AND $_REQUEST["action"] == "Update") {
        $status = $_REQUEST["action"];
        if (isset($_REQUEST["key"])) {
            //$action = $_REQUEST["action"];
            $blogKey = $_REQUEST["key"];
            $blogEntry = getEntry($blogKey, "blogs");
            $bTyp = $blogEntry["bTyp"];
            $bTitel = $blogEntry["bTitel"];
            $bTeaser = $blogEntry["bTeaser"];
            $bText = $blogEntry["bText"];
            $bTextIllu = $blogEntry["bTextIllu"];
            $bLink = $blogEntry["bLink"];
            $bLinkAbrufDat = $blogEntry["bLinkAbrufDat"];
            $bAutorKrz = $blogEntry["bAutorKrz"];
            $bAutorNam = $blogEntry["bAutorNam"];
            $bDatErsterfassung = $blogEntry["bDatErsterfassung"];
            $bDatLastUpdate = $blogEntry["bDatLastUpdate"];
            $bSymbolbild = $blogEntry["bSymbolbild"];
        }
    }
    
    if (isset($_REQUEST["action"]) AND $_REQUEST["action"] == "NEW") {
        $status = $_REQUEST["action"];
        $bTyp = "info";
        $blogKey = date("Ymd-His");
        $bDatErsterfassung = date("Y-m-d H:i");
        $bLinkAbrufDat = date("Y-m-d H:i");
        $bDatLastUpdate = "";
        $bSymbolbild = "images/blog-101.jpg";
    }
}
if ($status == "Update" OR $status == "NEW") {
    $bDatLastUpdate = date("Y-m-d H:i");
    
?>
<div id="blog-single">

  <div class="container" style="margin-top: 100px;">
   <!--Row Start-->
    <div class="row">
        <div class="col-sm-12">
			<h1 data-aos="fade-left" data-aos-delay="300"><?= $status ?> Blogbeitrag</h1>
<?php           if ($isOK == "OK") {
                    echo '
                        <div class="alert alert-success" role="alert">
                          <a href="index.php#jumpBlogs" class="btn btn-success">OK</a> gespeichert. 
                        </div>
                    ';
                }
?>
		</div>

            <section>
                <div class="container">
                  <!-- Grid row-->
                    <div class="row">
            		<div class="col-sm-12">
            
                    	<form method="post" action="" id="frm_blog">
                    		<input type="hidden" name="action" value="<?= $status ?>">                    		
                    		<input type="hidden" name="bKey" value="<?= $blogKey ?>">                    		
                    		<input type="hidden" name="bTyp" value="<?= $bTyp ?>">                    		
                    		<input type="hidden" name="bAutorKrz" value="<?= $bAutorKrz ?>">                    		
                    		<input type="hidden" name="bAutorNam" value="<?= $bAutorNam ?>">                    		
                    		<input type="hidden" name="bDatLastUpdate" value="<?= $bDatLastUpdate ?>">                    		
                    
                            <div class="input-group mb-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text" id="DatErsterfassung" style="width: 120px;">Erfasst am</span>
                              </div>
                        		<input type="text" class="form-control" name="bDatErsterfassung" 
                        			   value="<?= $bDatErsterfassung ?>">
                            </div>
                            	
                            <div class="input-group mb-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text" id="symbolbildURL" style="width: 120px;">Symbolbild</span>
                              </div>
                              <input type="text" class="form-control" name="bSymbolbild" id="bSymbolbild" 
                              		 aria-describedby="symbolbildURL" value="<?= $bSymbolbild ?>" >
                              <img src="<?= $bSymbolbild ?>" id="bSymbolbildImg" width="40" height="40">
                               	<script>
                              		function setValues(selVal) {
                              			document.getElementById('bSymbolbildImg').src = selVal;
                              			document.getElementById('bSymbolbild').value = selVal;
                              		}
                              	</script>
                                <div class="btn-group">
                                  <button type="button"  style="width: 40px;" class="btn btn-primary dropdown-toggle" 
                                  		  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  </button>
                                  <div class="dropdown-menu">
                                  	<?php for ($i = 101; $i < 120; $i++) { ?>
                    	                <a class="dropdown-item" href="#" onClick="setValues('images/blog-<?= $i ?>.jpg')"><img src="images/blog-<?= $i ?>.jpg" width="23" height="23"><?= $i ?></a>
                                  	<?php } ?>
                                  </div>
                                </div>
                            </div>
                            	
                            <div class="input-group mb-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text" id="titel" style="width: 120px;">Titel</span>
                              </div>
                              <input type="text" name="bTitel" id="bTitel" class="form-control" 
                              		 aria-label="Titel" aria-describedby="titel" value="<?= $bTitel ?>">
                            </div>
                            
                            
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text" style="width: 120px; vertical-align: top;">Teaser</span>
                              </div>
                              <textarea name="bTeaser" id="bTeaser" class="form-control" aria-label="Teaser" 
                              			style="height: 120px;"><?= $bTeaser ?></textarea>
                            </div>
                            
                            <br>
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text" style="width: 120px;">Text</span>
                              </div>
                              <textarea name="bText" class="form-control" aria-label="Text" 
                              			style="height: 186px;"><?= $bText ?></textarea>
                            </div>
                            
                            <br>
                            <label for="symbolbild">Link zu einer Grafik oder sonst ein Link im Netz</label>
                            <div class="input-group mb-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text" id="textIllu" style="width: 120px;">https://...</span>
                              </div>
                              <input type="text" name="bTextIllu"  id="bTextIllu" class="form-control" 
                              		 aria-describedby="textIllu" value="<?= $bTextIllu ?>">
                            </div>
                            
                            
                            <label for="symbolbild">Weiterführende Informationen</label>
                            <div class="input-group mb-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text" id="link" style="width: 120px;">https://...</span>
                              </div>
                              <input type="text" name="bLink" id="bLink"  class="form-control"
                              		 aria-describedby="link" value="<?= $bLink ?>">
                            </div>
                            
                            	
                            <div class="input-group mb-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text" id="LinkAbrufDat" style="width: 170px;">Link-Abrufdatum</span>
                              </div>
                              <input type="text" name="bLinkAbrufDat" id="bLinkAbrufDat" class="form-control" 
                              		 aria-label="Username" aria-describedby="LinkAbrufDat" value="<?= $bLinkAbrufDat ?>">
                            </div>
                            
                            <br>
                    			<a href="index.php#jumpBlogs" class="btn btn-success" style="width: 200px;">zurück zur Blog-Liste</a></li>
                    			<input type="submit" name="btn_speichern" value="Speichern" 
                    					   class="btn btn-primary special" style="width: 120px; margin-left: 10px;"/></li>
                    		
                            <br>		
                            <br>		
                            <br>		
                    	</form>
            
                	  </div>
                    </div>
                </div>
            </section>

		</div>
	</div>
  </div>

    
<?php     
}

if ($status == "View") {
    $key = "";
    if (isset( $_REQUEST["key"] )) {
        $key =  $_REQUEST["key"];
    }
    
    $blogValue = getEntry($key, "blogs");
    //echo "<pre>".print_r($blogValue,TRUE)."</pre>";
    $bDate = $blogValue["bDatErsterfassung"];
    if (strlen($blogValue["bDatLastUpdate"]) > 5) {
        $bDate = $blogValue["bDatLastUpdate"].' updated';
    }
    $link = '';
    if (strlen($blogValue["bLink"]) > 3) {
        $link = '<a href="'.$blogValue["bLink"].'">'.$blogValue["bLink"].'</a>
    <br><span style="font-size: 9pt; letter-spacing: 1.25px;">(abgerufen am: '.$blogValue["bLinkAbrufDat"].')</span>';
    }
?>

<section class="banner-home"></section>
<!-- Banner section End-->

<!-- blog-single Section Start -->
<div id="blog-single">
    <div class="container">
		<p><a href="index.php#jumpBlogs" class="btn btn-success">zurück zur Blog-Liste</a></p>
    	<hr>
    	<h4>Blogbeitrag</h4>
		<h1 data-aos="fade-left" data-aos-duration="500"><?= $blogValue["bTitel"] ?></h1>
    </div>
    <hr/>
    <div class="container">
        <div class="heading">
            <h2><?= $blogValue["bTeaser"] ?></h2>
        </div>
          <div class="row" data-aos="fade-up" data-aos-duration="700">
             <div class="col-sm-12 col-lg-12 box">
              <img src="<?= $blogValue["bSymbolbild"] ?>" alt="" class="img-thumbnail image">
              <p data-toggle="tooltip" data-placement="top" title="<?= $blogValue["bAutorNam"] ?>">[<?= $blogValue["bAutorKrz"] ?>] <?= $bDate ?></p>
              <p><?= $blogValue["bText"] ?> </p>
              <?php 
                if (isset($blogValue["bTextIllu"]) OR strlen($blogValue["bTextIllu"]) > 4) {
                    $bTextIllu = $blogValue["bTextIllu"];
                    if (   (substr($bTextIllu, -4) == ".jpg") 
                        OR (substr($bTextIllu, -4) == ".png") 
                        OR (substr($bTextIllu, -4) == ".gif") ) { 
                        echo '<p><img src="'.$bTextIllu.'" style="height: 200px;"></p>';  
                    } else { 
                        echo '<p><a href="'.$bTextIllu.'" target="_blank">'.$bTextIllu.'</a></p>';
                    }
                } else {
                    echo 'no bTextIllu';
                }
                if (strlen($link) > 3) {
                    echo '<p>Link / Referenz: '.$link;
                }
              ?>
<!--               <div class="quotation"> -->
<!--               		<h4> Kasten Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed <br/>do eiusmod  tempor incididunt ut labore et dolore magna <br/>aliqua. sed  do eiusmod tempor incididunt ut labore et dolore<br/> magna aliqua..</h4> -->
<!--               </div> -->
            </div>
          </div>

 <?php include 'inc.blogs-writecomment.php' ?>

 <?php //include 'inc.blogs-more-similar.php' ?>
      
    </div>
</div>
<!-- Blog_single Section End -->

<?php include 'inc.subscribe.php' ?>

<?php  
} 
?>

<?php include 'inc.footer.php' ?>