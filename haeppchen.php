<?php include 'inc.header.php'; ?>

<section id="items" >

    <div class="container">
         <div class="heading" data-aos="fade-up" data-aos-delay="300">
            <br>
            <br>
            <br>
            <br>
            <h2>Häppchen</h2>
            <h3>"Begriffsklärung", "Glossar", "Vokabular", "Wörterverzeichnis"</h3>
            <p>Wir erklären in kleinen Häppchen die gängigen Begriffe die Insider benutzen, damit ihr endlich auch mitreden könnt.</p> 
         </div>
         

      <div class="row text-left text-lg-left">
        <div class="col-lg-2 col-md-4 col-6" data-aos="fade-left" data-aos-delay="100" style="width: 200px;">
      		<a href="https://youtu.be/ssK2ZqbvB0k" target="_blank">Was ist ein Asset?</a>
            <audio controls="controls" style="width: 200px; height: 24px;">
				<source src="podcast/asset-a.mp4" title="Asset">
            </audio>
        </div>		

        <div class="col-lg-2 col-md-4 col-6" data-aos="fade-left" data-aos-delay="200">
      		<a href="https://youtu.be/ssK2ZqbvB0k" target="_blank">Blockchain</a>
        </div>		

        <div class="col-lg-2 col-md-4 col-6" data-aos="fade-left" data-aos-delay="300">
      		<a href="https://youtu.be/ssK2ZqbvB0k" target="_blank">Bären, bärisch</a>
        </div>		

        <div class="col-lg-2 col-md-4 col-6" data-aos="fade-left" data-aos-delay="400">
      		<a href="https://youtu.be/ssK2ZqbvB0k" target="_blank">Bulle, bullisch</a>
        </div>		

        <div class="col-lg-2 col-md-4 col-6" data-aos="fade-left" data-aos-delay="500">
      		<a href="https://youtu.be/ssK2ZqbvB0k" target="_blank">Cake</a>
        </div>		

        <div class="col-lg-2 col-md-4 col-6" data-aos="fade-left" data-aos-delay="600">
      		<a href="https://youtu.be/ssK2ZqbvB0k" target="_blank">Coin</a>
        </div>		

        <div class="col-lg-2 col-md-4 col-6" data-aos="fade-left" data-aos-delay="700">
      		<a href="https://youtu.be/ssK2ZqbvB0k" target="_blank">DeFi</a>
        </div>		

        <div class="col-lg-2 col-md-4 col-6" data-aos="fade-left" data-aos-delay="800">
      		<a href="https://youtu.be/ssK2ZqbvB0k" target="_blank">Ether, Etherium</a>
        </div>		

        <div class="col-lg-2 col-md-4 col-6" data-aos="fade-left" data-aos-delay="900">
      		<a href="https://youtu.be/ssK2ZqbvB0k" target="_blank">Hacks</a>
        </div>		

        <div class="col-lg-2 col-md-4 col-6" data-aos="fade-left" data-aos-delay="1000">
      		<a href="https://youtu.be/ssK2ZqbvB0k" target="_blank">Swap</a>
        </div>

        <div class="col-lg-2 col-md-4 col-6" data-aos="fade-left" data-aos-delay="1100">
      		<a href="https://youtu.be/ssK2ZqbvB0k" target="_blank">Token</a>
        </div>

        <div class="col-lg-2 col-md-4 col-6" data-aos="fade-left" data-aos-delay="1200">
      		<a href="https://youtu.be/ssK2ZqbvB0k" target="_blank">Wale</a>
        </div>

      </div>
	  
	  Weitere Kryptowörter:
	  <br><a href="https://www.techfacts.de/ratgeber/krypto-vokabular-grundbegriffe-die-man-wissen-muss" target="_blank">techfacts.de</a>


    </div>
    <!-- /.container -->
</section>
<!-- Items section Ended-->

<?php //include 'inc.subscribe.php' ?>

<?php include 'inc.footer.php' ?>

<script>
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
</script>
