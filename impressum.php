<?php include 'inc.header.php'; ?>

<section class="banner-home">
<!-- Banner section Start-->

   <!-- container Start-->
    <div class="container">
       <!--Row Start-->
        <div class="row">
            <div class="col-sm-12">
            <br><br>
                <h1 data-aos="fade-left" data-aos-delay="300">Impressum</h1>
                <p>
                <br>
                <br>
                <br>
                <br>COINDITOREI
                <br>Rainstrasse 27
                <br>CH-8610 Uster
                <br>
                <br>
                <br>
                <br>
               <form name="frm_mail" method="post" action=""> 
                <p class="haupttext">Email:
                <br>
                <?php 
                $mailadresse = "";
                if (isset($_REQUEST["checkWert"])) {
                    if ($_REQUEST["checkWert"] == "1") {
                        $mailadresse = '<a href="mailto:'.$APP_EMAIL.'">'.$APP_EMAIL.'</a><br>
							<a href="https://wa.me/41797001414" target="_blank">
							<img src="images/icons/whatsapp_icon.png" style="border-radius: 24px;"></a>
                            ';
                    }
                }
                if (strlen($mailadresse) > 3) {
                    echo $mailadresse;
                } else { ?>
                    <img src="images/captcha.jpg" style="height: 36px;"> =
                    <input type="text" name="checkWert" style="height: 37px; width: 50px;">
                    <input type="submit" name="btn_submit" value="OK" class="btn btn-primary" style="margin-bottom: 2px;">
                <?php 
                }
                ?>
                <br>
                <br>
                Bilder: Shutterstock.com
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                </p>
            </div>
        </div>
       <!--Row Ended-->
    </div>
   <!-- container Ended-->
</section>
<!-- Banner section Ended-->

<?php //include 'inc.subscribe.php' ?>

<?php include 'inc.footer.php' ?>
