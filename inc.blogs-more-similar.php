
      <section class="more-blogs">
        <div class="row">
           <div class="col-sm-12">
               <button type="button" class="btn btn-primary" disabled>Similar Posts</button>
           </div>
        </div>
         <div class="row" data-aos="fade-up" data-aos-delay="500">
             <div class="col-lg-6 col-sm-6">
                 <div class="row">
                     <div class="col-lg-5 col-sm-12">
                        <a href="blog_single.php" class="d-block mb-5 h-100">
                          <img class="img-fluid img-thumbnail" src="images/blog-1.jpg" alt="blog-1">
                        </a>
                     </div>
                     <div class="col-lg-7 col-sm-12 inner-content">
                         <h5><a href="blog_single.php">Beautiful pamogranate cutted to taste for the Drink in cup</a></h5>
                         <h6>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of </h6>
                     </div>
                 </div>
             </div>
             <div class="col-lg-6 col-sm-6">
                 <div class="row">
                     <div class="col-lg-5 col-sm-12">
                        <a href="blog_single.php" class="d-block mb-5 h-100">
                          <img class="img-fluid img-thumbnail" src="images/blog-2.jpg" alt="blog-2">
                        </a>
                     </div>
                     <div class="col-lg-7 col-sm-12 inner-content">
                         <h5><a href="blog_single.php">Beautiful pamogranate cutted to taste for the Drink in cup</a></h5>
                         <h6>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of </h6>
                     </div>
                 </div> 
             </div>
         </div>
      </section>
      
