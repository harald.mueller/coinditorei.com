<?php include 'inc.header.php'; ?>

<section class="banner-home">

    <div class="container">
       <!--Row Start-->
        <div class="row">
            <div class="col-sm-12">
                <h1 data-aos="fade-left" data-aos-delay="300">Willkommen in der </h1><br>
                <h2 data-aos="fade-left" data-aos-delay="500">COINDITOREI</h2>
                <h4 data-aos="fade-left" data-aos-delay="700">Der Info-Shop über Kryptowährungen. 
														<span>Wir schauen den Coins ins Innere. Auf und in die
														  <br>Tortenböden, Füllungen, Crèmen, Couverturen usw.</span></h4>        
				<div>
                     <div class="hid schwebende-coins anim-delay0">
                    	<img src="images/coins/1.png" >
                    </div>
                    <div class="hid schwebende-coins animation-reverse anim-delay1">
                    	<img src="images/coins/1027.png" >
                    </div> 
                    <div class="hid schwebende-coins anim-delay2">
                    	<img src="images/coins/5426.png" >
                    </div>     
                    <div class="hid schwebende-coins animation-reverse anim-delay3">
                    	<img src="images/coins/2010.png">
                    </div>     
                    <div class="hid schwebende-coins anim-delay4">
                    	<img src="images/coins/52.png">
                    </div>     
                    <div class="hid schwebende-coins animation-reverse anim-delay5">
                    	<img src="images/coins/1839.png">
                    </div>     
				</div> 
                <p>&nbsp;<br>&nbsp;</p>
                <p>&nbsp;<br>&nbsp;</p>

<!--            <a class="btn-success" href="index.php#jumpBlogs" role="button" data-aos="fade-right" data-aos-delay="100">Blog</a> -->
                <a class="btn-success" href="https://coinditorei.ch/wp" role="button" data-aos="fade-right" data-aos-delay="100">Blog</a>
				
                <a class="btn-success" href="vitrine" role="button" data-aos="fade-right" data-aos-delay="300">Vitrine</a>
                <a class="btn-success" href="rezepte.php" role="button" data-aos="fade-right" data-aos-delay="1000">Rezepte</a>
                <a class="btn-success" href="haeppchen.php" role="button" data-aos="fade-right" data-aos-delay="1600" title="Begriffsklärung, Glossar, Vocabular, Wörterliste" >Häppchen</a>

                <br>
                <br>
                <a href="https://coinditorei.com/wp/?page_id=51" class="btn btn-danger" data-aos="fade-right" data-aos-delay="1200">Mitglieder-Bereich ...</a>
                <a href="https://coinditorei.com/wp/?page_id=21" class="btn btn-warning" data-aos="fade-right" data-aos-delay="1800">Kurse ...</a>

				
            </div>
        </div>
       <!--Row Ended-->
    </div>
   <!-- container Ended-->
</section>
<!-- Banner section Ended-->

<?php include 'inc.blogs.php' ?>

<?php //include 'inc.testimonial.php' ?>

<?php //include 'inc.ueber.php' ?>

<?php //include 'inc.items.php' ?>

<?php //include 'inc.subscribe.php' ?>

<?php include 'inc.footer.php' ?>

<script>
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
</script>
