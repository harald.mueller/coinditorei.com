<?php
$zaehler = 0;
$dir_in = "in/";
$dir_out = "out/";
$logfile = "log.txt";
$trace = "";
$archiveTime = date('Ymd_His');
$receiptfile = "quittungsfile".$archiveTime.".txt";
if ( is_dir( $dir_in )) {// Test, ob ein Verzeichnis angegeben wurde
	if ( $handle = opendir($dir_in) ) {// Oeffnen des Verzeichnisses
		while ( ($firmendir = readdir($handle)) !== false) {// einlesen der Verzeichnisse
			if (substr($firmendir,0,1)!=".") {//nur Files oder Dirs
				$zaehler++;
				//echo "\n".$firmendir." ".$zaehler;
				if(is_dir($dir_in.$firmendir)) {
					//echo "\n".$dir_in.$firmendir;
					if ( $innerhandle = opendir( $dir_in.$firmendir ) ) {// Oeffnen des Verzeichnisses
						while ( ($innerfile = readdir($innerhandle)) !== false) {// einlesen der Verzeichnisse
							if (
								($innerfile != "readme.txt") 
								 AND ($innerfile != "index.php")  
								 AND (substr($innerfile,0,1) != "."
								 AND ($innerfile != "logs")  
								 AND ($innerfile != "archive")  
								) 
							   ) 
							{
								$invoiceFile = $dir_in.$firmendir."/".$innerfile;
								//echo "\ninvoiceFile:".$invoiceFile;
								$logDir = $dir_in.$firmendir."/logs";
								$archivDir = $dir_in.$firmendir."/archive";
								$archivDirTime = $dir_in.$firmendir."/archive/".$archiveTime;
								
								if ((substr($innerfile,-11) == "invoice.txt") OR (substr($innerfile,-11) == "invoice.xml")) {
									$trace .= "<br>Verarbeitung: ".$innerfile;
									
									checkDirectory($logDir);								
									copy($dir_in."index.php",$logDir."/index.php");
									checkDirectory($archivDir);
									copy($dir_in."index.php",$archivDir."/index.php");
									checkDirectory($archivDirTime);
									copy($dir_in."index.php",$archivDirTime."/index.php");
									
									if (substr($innerfile,-11) == "invoice.xml") {
										$infos = processInvoiceXML($dir_in.$firmendir."/".$innerfile);
									} else {
										$infos = processInvoiceTXT($dir_in.$firmendir."/".$innerfile);
									}
									$logMaterial = $innerfile." verarbeitet -- ";
									$logMaterial .= $infos;
									
									writeAFile($logDir."/".$logfile, $logMaterial);
									
									writeAFile($dir_out.$firmendir."/".$receiptfile, $logMaterial);
									$trace.= "  => ".$dir_out.$firmendir."/".$receiptfile;
									
									rename($dir_in.$firmendir."/".$innerfile, $archivDirTime."/".$innerfile);
								}
							}
						}//end while
					}
				}//end if(is_dir())

			}
		}//end while
		closedir($handle);
	}// end if
}// end if
if (strlen($trace) == 0) {
	echo "Keine Verarbeitung";
} else {
	echo $trace;
}
echo "<br><br><a href='index.php'>Zurück</a>";

function writeAFile($filename, $logMaterial) {
	if (!$ausgabeDatei = fopen($filename, 'a')) {
		print "Kann die Datei ".$filename." nicht öffnen";
        exit;
	}
	$s = date('Ymd-His')."  ".$logMaterial."\n" ;
	if (fwrite($ausgabeDatei, $s) === FALSE) {
        print "Kann nicht in die Datei $filename schreiben";
        exit;
    }
	fclose($ausgabeDatei);
}

function checkDirectory($aDir) {
	if (!is_dir($aDir)) {
		@mkdir($aDir);
		//echo "\n".$aDir." erzeugt";
	}
}

function processInvoiceTXT($invoiceTXT) {
	$lines = file($invoiceTXT);
	$relevanteInfos = "";
	foreach ($lines as $line_num => $line) {
		if ($line_num > 2 AND $line_num < 11 AND strlen(trim($line)) > 2 ) {
			$relevanteInfos .= trim($line).";";			
		} 
		if(stristr($line, 'Kundennummer')) {
			$relevanteInfos .= trim($line).";";
		}
		if(stristr($line, 'Rechnung')) {
			$relevanteInfos .= trim($line).";";
		}
		if(stristr($line, 'Total')) {
			$relevanteInfos .= trim($line)."; ";
		}
		if(stristr($line, 'Zahlungsziel')) {
			$relevanteInfos .= trim($line);
		}
	}
	return str_replace(" ","",$relevanteInfos);
}

function processInvoiceXML($invoiceXML) {
	$lines = file($invoiceXML, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
	$relevanteInfos = "";
	foreach ($lines as $line_num => $line) {
		//echo $line_num." ".$line;
		if(stristr($line, 'Reference-No>')) {
			$clearLine = str_replace("Reference-No>","",$line);
			$clearLine = str_replace("<","",$clearLine);
			$clearLine = str_replace(" ","",$clearLine);
			if ($clearLine != "/") {
			    $relevanteInfos .= "Ref:".$clearLine;
			}
		}
		if(stristr($line, '<Amount>')) {
			$clearLine = str_replace("Amount>","",$line);
			$clearLine = str_replace("<","",$clearLine);
			$clearLine = str_replace(" ","",$clearLine);
			if ($clearLine != "/") {
			    $relevanteInfos .= "Amt:".$clearLine;
			}
		}
	}
	return $relevanteInfos;
}

?>