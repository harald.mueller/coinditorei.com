# erstellt alle Verzeichnisse und kopiert die 
# Template-Dateien in alle Teilnehmer aller Klassen

$verzeichnis_template_in = "in\_template"
$verzeichnis_template_out = "out\_template"
$verzeichnis_namensdateien = "_namensdateien"

$allFiles = (dir $verzeichnis_namensdateien).name 

foreach ($file in $allFiles) {
    $fileNameVorPunkt = $file.split(".")[0]
    $klasse = $fileNameVorPunkt.split("-")[1]
    $fileinhalt = type $verzeichnis_namensdateien/$file
    foreach ($teilnehmer in $fileinhalt) {
        mkdir "in\$klasse$teilnehmer"
        copy $verzeichnis_template_in/*.* "in\$klasse$teilnehmer"
        mkdir "out\$klasse$teilnehmer"
        copy $verzeichnis_template_out/*.* "out\$klasse$teilnehmer"
    }
}
