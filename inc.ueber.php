<?php
?>

<!-- About section Ended-->
<section class="ueber">
    <!-- Page Content -->
    <div class="container">
       <div class="row">
           <div class="col-lg-6 col-sm-12" data-aos="fade-right" data-aos-delay="300">
                <div class="schwebende-coins">
                	<img src="images/coins/1027.png" >
                </div>                
                <div class="schwebende-coins animation-reverse anim-delay1">
                	<img src="images/coins/52.png" >
                </div>     
                <div class="schwebende-coins anim-delay2">
                	<img src="images/coins/2.png" >
                </div>     
                <div class="schwebende-coins animation-reverse anim-delay3">
                	<img src="images/coins/2010.png" >
                </div>     
                <div class="schwebende-coins anim-delay4">
                	<img src="images/coins/5426.png" >
                </div>     
                <div class="schwebende-coins animation-reverse anim-delay5">
                	<img src="images/coins/1.png" >
                </div>
                <a href="#" class="d-block">
                	<img class="img-fluid img-thumbnail" src="images/ueber-bg.jpg" alt="ueber-bg">
                </a>
           </div>
           <div class="col-lg-6 col-sm-12">
              <div class="row" data-aos="fade-left" data-aos-delay="500">
                  <div class="col-sm-12">
                      <h2>Über die <span>Coinditorei</span></h2>
                      <p>One of the finest food and restaurant theme on the net, cleaniest and simplistic wordpress theme, browse it and experience it Lorem Ipsum is simply dummy text of the printing and typesetting </p>
                      <p>Dummy text ever since the 1500s, when an unknown printer took a galley of type and</p>

                  </div>
              </div>
              <div class="row">
                  <div class="col-6 col-md-4" data-aos="fade-up" data-aos-delay="700">
                      <ul>
                          <li><i class="fa fa-bookmark-o" aria-hidden="true"></i><a href="about.php">Bakings</a> </li>
                          <li><i class="fa fa-bookmark-o" aria-hidden="true"></i><a href="about.php">Breakfast</a> </li>
                          <li><i class="fa fa-bookmark-o" aria-hidden="true"></i><a href="about.php">brunch</a> </li>
                      </ul>
                  </div>
                  <div class="col-6 col-md-4" data-aos="fade-up" data-aos-delay="800">
                      <ul>
                          <li><i class="fa fa-bookmark-o" aria-hidden="true"></i><a href="about.php">salads</a> </li>
                          <li><i class="fa fa-bookmark-o" aria-hidden="true"></i><a href="about.php">recipes</a> </li>
                          <li><i class="fa fa-bookmark-o" aria-hidden="true"></i><a href="about.php">sweets pizzas</a> </li>
                      </ul>
                  </div>
                  <div class="col-6 col-md-4" data-aos="fade-up" data-aos-delay="900">
                      <ul>
                          <li><i class="fa fa-bookmark-o" aria-hidden="true"></i><a href="about.php">Bakings</a> </li>
                          <li><i class="fa fa-bookmark-o" aria-hidden="true"></i><a href="about.php">Breakfast</a> </li>
                          <li><i class="fa fa-bookmark-o" aria-hidden="true"></i><a href="about.php">brunch</a> </li>
                      </ul>
                  </div>
              </div>
              <div class="row">
                  <div class="col-sm-12" data-aos="fade-up" data-aos-delay="700">
                      <a class="btn btn-primary" href="about.php" role="button">+ many more...</a>
                  </div>
              </div>
           </div>
       </div>
    </div>
    <!-- /.container -->
</section>
<!-- About section Ended-->
