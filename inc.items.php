<?php
?>
 
<!-- Items section Ended-->
<section class="items">
    <!-- Page Content -->
    <div class="container">
         <div class="heading" data-aos="fade-up" data-aos-delay="300">
            <h2>Check Out <span><a href="recipes.php">My Recipes</a></span> and Food Items</h2>
            <h3>One of the finest food and restaurant theme on the net, cleaniest and simplistic wordpress theme, browse it and experience it</h3>
         </div>

      <div class="row text-center text-lg-left">

        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-left" data-aos-delay="300">
         <div class="imageBox">
            <a href="images/item-1.jpg" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
              <img class="img-fluid img-thumbnail" src="images/item-1.jpg" alt="">
              <div class="textBox">
                  <h5>Sea Food</h5>
              </div>
            </a>
         </div>
        </div>
        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-left" data-aos-delay="500">
         <div class="imageBox">
          <a href="images/item-2.jpg" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
            <img class="img-fluid img-thumbnail" src="images/item-2.jpg" alt="">
              <div class="textBox">
                  <h5>Main Dishes</h5>
              </div>
          </a>
         </div>
        </div>
        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-left" data-aos-delay="700">
         <div class="imageBox">
          <a href="images/item-3.jpg" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
            <img class="img-fluid img-thumbnail" src="images/item-3.jpg" alt="">
              <div class="textBox">
                  <h5>Appetizers</h5>
              </div>
          </a>
         </div>
        </div>
        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-left" data-aos-delay="900">
         <div class="imageBox">
          <a href="images/item-4.jpg" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
            <img class="img-fluid img-thumbnail" src="images/item-4.jpg" alt="">
              <div class="textBox">
                  <h5>Desserts</h5>
              </div>
          </a>
         </div>
        </div>
        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-right" data-aos-delay="300">
         <div class="imageBox">
          <a href="images/item-5.jpg" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
            <img class="img-fluid img-thumbnail" src="images/item-5.jpg" alt="">
              <div class="textBox">
                  <h5>Drinks</h5>
              </div>
          </a>
         </div>
        </div>
        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-right" data-aos-delay="500">
         <div class="imageBox">
          <a href="images/item-6.jpg" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
            <img class="img-fluid img-thumbnail" src="images/item-6.jpg" alt="">
              <div class="textBox">
                  <h5>Steak Food</h5>
              </div>
          </a>
         </div>
        </div>
        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-right" data-aos-delay="700">
         <div class="imageBox">
          <a href="images/item-7.jpg" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
            <img class="img-fluid img-thumbnail" src="images/item-7.jpg" alt="">
              <div class="textBox">
                  <h5>Sea Food</h5>
              </div>
          </a>
         </div>
        </div>
        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-right" data-aos-delay="900">
         <div class="imageBox">
          <a href="images/item-8.jpg" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
            <img class="img-fluid img-thumbnail" src="images/item-8.jpg" alt="">
              <div class="textBox">
                  <h5>Drinks</h5>
              </div>
          </a>
         </div>
        </div>
        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-left" data-aos-delay="300">
         <div class="imageBox">
          <a href="images/item-4.jpg" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
            <img class="img-fluid img-thumbnail" src="images/item-4.jpg" alt="">
              <div class="textBox">
                  <h5>Desserts</h5>
              </div>
          </a>
         </div>
        </div>
        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-left" data-aos-delay="500">
         <div class="imageBox">
          <a href="images/item-2.jpg" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
            <img class="img-fluid img-thumbnail" src="images/item-2.jpg" alt="">
              <div class="textBox">
                  <h5>Appetizers</h5>
              </div>
          </a>
         </div>
        </div>
        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-left" data-aos-delay="700">
         <div class="imageBox">
          <a href="images/item-3.jpg" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
            <img class="img-fluid img-thumbnail" src="images/item-3.jpg" alt="">
              <div class="textBox">
                  <h5>Main Dishes</h5>
              </div>
          </a>
         </div>
        </div>
        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-left" data-aos-delay="900">
         <div class="imageBox">
          <a href="images/item-1.jpg" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
            <img class="img-fluid img-thumbnail" src="images/item-1.jpg" alt="">
              <div class="textBox">
                  <h5>Other Food</h5>
              </div>
          </a>
         </div>
        </div>
      </div>
   
    </div>
    <!-- /.container -->
</section>
<!-- Items section Ended-->
