<?php 
session_start();
include 'inc.header.php';
$isOK = FALSE;
$kurzzeichen = "";
$anzeigename = "";
$email = "";
if (isset($_REQUEST["btn_ok"])) {
    if ($_REQUEST["password"] != $_REQUEST["password2"]) {
        $isOK = "datumwoerterUngleich";
        $kurzzeichen = $_REQUEST["kurzzeichen"];
        $anzeigename = $_REQUEST["anzeigename"];
        $email = $_REQUEST["email"];
    } else {
        if (isset($_REQUEST["btn_ok"])) {
            //echo '<pre>'.print_r($_REQUEST, TRUE).'</pre>';
            $kurzzeichen = htmlentities($_REQUEST["kurzzeichen"]);
            $anzeigename = htmlentities($_REQUEST["anzeigename"]);
            $email = $_REQUEST["email"];
            $userArr = array();
            $userListArr = getJsonData("users");
            $userArr["uRolle"] = "coinditor";
            $userArr["uKurzzeichen"] = $kurzzeichen;
            $userArr["uAnzeigename"] = $anzeigename;
            $userArr["udatumword"] = password_hash($_REQUEST["password"], PASSWORD_DEFAULT);
            $userListArr[$_REQUEST["email"]] = $userArr;
            $isOK = saveList($userListArr, "users");
            $isOK = "OK";
        }
    }
}
?>
<form action="" method="post">
<section class="banner-home">
    <div class="container" style="margin-top: 40px;">
       <!--Row Start-->
        <div class="row">
            <div class="col-sm-6">
                <h1 data-aos="fade-left" style="margin: 50px 0;">Registrierung</h1>
<?php           if ($isOK == "OK") {
                    echo '
                        <div class="alert alert-success" role="alert">
                          <b>OK</b>. Registrierung erfolgreich.<br>Sie können nun das <a href="login.php" class="alert-link">Login</a> machen.
                        </div>
                    ';
                }
                if ( $isOK == "datumwoerterUngleich") {
                    echo '
                        <div class="alert alert-danger" role="alert">
                          Die beiden datumwörter stimmen nicht überein.
                        </div>
                    ';
                }
?>                
                
                <div class="input-group" style="padding: 20px 20px 20px 0;">
                  <div class="input-group-prepend mb-3">
                    <span class="input-group-text" style="width: 120px;">Kurzzeichen:</span>
            		<input type="text" id="kurzzeichen" name="kurzzeichen" 
            			   style="width: 250px;" class="form-control" aria-label="Large" value="<?= $kurzzeichen ?>">
                  </div>
                  <div class="input-group-prepend mb-3">
                    <span class="input-group-text" style="width: 120px;">Name:</span>
            		<input type="text" id="anzeigename" name="anzeigename" 
            			   style="width: 250px;" class="form-control" aria-label="Large" value="<?= $anzeigename ?>">
                  </div>
                  <div class="input-group-prepend mb-3">
                    <span class="input-group-text" style="width: 120px;">Email:</span>
            		<input type="email" id="email" name="email" 
            			   style="width: 250px;" class="form-control" aria-label="Large" value="<?= $email ?>">
                  </div>
                  <div class="input-group-prepend mb-3">
                    <span class="input-group-text" style="width: 120px;">datumwort:</span>
            		<input type="password" id="password" name="password" 
            			   style="width: 250px;" class="form-control" aria-label="Large">
                  </div>
                  <div class="input-group-prepend mb-3">
                    <span class="input-group-text" style="width: 120px;">datumw. wdh.:</span>
            		<input type="password" id="password2" name="password2" 
            			   style="width: 250px;" class="form-control" aria-label="Large">
                  </div>
                  <div class="input-group mb-3" style="margin: 20px 0 150px 0;">
        			<button name="btn_ok" class="btn btn-primary btn-lg" 
        				    style="width: 370px;">OK</button>
                  </div>
                </div>
                    
    		</div>
        </div>
	</div>
</section>
</form>

<?php include 'inc.footer.php'; ?>