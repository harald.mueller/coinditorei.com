<?php
?>
<!-- Testimonial section -->
<section class="testimonial">
 <div id="carousel">    				
	<div class="container">
	<div class="quote" style="float: left;"><i class="fa fa-quote-left fa-5x" aria-hidden="true"></i></div>
		<div class="row">
           <div class="col-sm-12">
          	 <p>Testimonial</p>
              <div class="heading" data-aos="fade-up" data-aos-delay="300">
                 <h2>Alle lieben <span>unsere Patisserie</span></h2>
                 <h3>Macaron, Éclair, Choux, Praline, Schokolade, Pâte de Fruits, Sablé, Tarte, Cake, Törtchen, Hochzeitstorte, Verrine, Bûche de Noel</h3>
              </div>
           </div>
            
			<div class="col-md-6 col1" data-aos="fade-right" data-aos-delay="500">
			<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				<div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
				  <!-- Carousel items -->
				  <div class="carousel-inner" style="text-align: right">
				    <div class="active item">
				    	<blockquote>
				    		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and</p>
				    	</blockquote>
                           <div class="text1">Bernhard Melmer <span>Coinditor</span></div>
				    	   <div class="profile-circle1" style="background-color: rgba(0,0,0,.2); float: right;"></div>
				    </div>
				  </div>
				</div>
            <i class="fa fa-angle-double-up" aria-hidden="true"></i>
			</div>	
			
			<div class="col-md-6" data-aos="fade-left" data-aos-delay="500">
				<div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
				  <!-- Carousel items -->
				  <div class="carousel-inner">
				    <div class="active item">
				    	<blockquote>
				    		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and</p>
				    	</blockquote>
				    	<div class="profile-circle2" style="background-color: rgba(0,0,0,.2);"></div>
				    	<div class="text2">Harald Müller <span>Coinditor</span><br><span style="font-size:7pt;">lic.oec.publ. (Dipl. Wirtschaftsinformatiker)<span></div>
				    </div>
				  </div>
				</div>
			</div>				
		</div>
		<div class="quote"><i class="fa fa-quote-right fa-5x" aria-hidden="true"></i></div>
	</div>
 </div>
</section>
<!-- Testimonial section Ended-->
