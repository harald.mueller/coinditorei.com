<?php include_once 'inc.header.php'; ?>

<div id="rezeptedetail">
    <div class="container">
       <h3>Bitcoin fällt um 6 Prozent. Marktmanipulation oder doch nur gesunde Korrektur?
       </h3>
       <a class="btn btn-success" href="rezepte.php" role="button" data-aos="fade-up" data-aos-delay="1000">zurück zu den Coin-Rezepte</a>
        <br><br>
        <div class="row" style="margin-right: 0; margin-left: 0;">
            <div data-aos="fade-up" data-aos-duration="800">
             <img src="images/item-2.jpg" alt="about-bg" class="thumbnail image">
              <p><!-- START REZEPTE-BEITRAG -->
                [27.10.2021] Es ist noch früh am Morgen, die Pop-up Nachrichten auf meinem Handy verlauten Ungemach:     „Bitcoin crasht um 6 Prozent“, „Bitcoin Rally beendet? Droht ein neuer Bärenmarkt?“, „Bitcoin: eindeutige Zeichen für neues All Time High in 72 Stunden“, usw. Lasst mich doch in Frieden, denke ich mir, und trinke meinen Kaffee. Seit Jahren dieselben Horrormeldungen, irgendwie überflüssig. 
              </p>
              <p>Nachmittags finde ich dennoch die Zeit, den Kursverläufen auf Coinmarketcap zu folgen. Wenig verwundert sehe ich alle Kryptowährungen, aufgelistet mit roten Zahlen. Bitcoin -6 Prozent, die Altcoins bis zu -28 Prozent im Minus. Doch es gibt noch Hoffnung für den Tag! 
              </p>
              <p>1Inch und Shiba Inu sind mit 35 bzw. 60 Prozent deutlich im Plus, mir fällt dazu nur das berühmte gallische Dorf ein. Die Analyse der Vitaldaten dieser Kryptowerte spornt mich jedoch ganz unvermittelt an. 1Inch (https://coinmarketcap.com/de/currencies/1inch) hatte eine etwas längere Durststrecke, und machte nur wenig Bewegung seitwärts, wie eine Krabbe. Nach kurzer Recherche fand ich die Antwort: 1Inch wurde an einer neuen Börse gelistet, deshalb dieser „Listing Effekt“. Der Coin stieg innerhalb von wenigen Stunden um das Doppelte im Preis an, doch schon wenige Stunden danach erlosch das Feuer für 1Inch, und auch das Handelsvolumen reduzierte sich um 56 Prozent.
              </p>
              <p>Was war bei Shiba Inu los (https://coinmarketcap.com/de/currencies/shiba-inu) ? Nichts, rein gar nichts rechtfertigt aus technischer Sicht den 900 Prozent Kursanstieg innerhalb weniger Wochen. Die 60 Prozent von heute? Geschenkt! Shiba hat weder eine praktische Anwendung noch einen handfesten Nutzen, so die weitverbreitete Meinung. Doch Shiba besitzt eine loyale Community, eine Shiba Army, und ist inzwischen auf sämtlichen Kryptobörsen erhältlich. Das smarte Marketing für Shiba, geheimnisvolle Nachrichten über Großinvestoren, und auch via Social Media gestreute utopische Preisziele, beflügeln diesen Kryptowert nachhaltig. Das kann man gut finden, oder kategorisch ablehnen. Es funktioniert jedenfalls so gut, dass Shiba Inu aktuell auf Platz 10 der Rangliste nach Marktkapitalisierung liegt, und sich mit Dogecoin anlegen kann. Ein Hundekampf über mehrere Runden steht uns bevor, wenn Shiba auch noch ausgerechnet auf Robinhood gelistet wird. Was sagt uns das über den Kryptomarkt? Antwort: alles reine Nervensache...
              </p>
              <p>Doch zurück zum Bitcoin. Der Kryptomarkt ist der Innbegriff von Marktmanipulation aller Art. Und auch an diesem Tag ist zu lesen, dass offensichtlich die 50 größten „Wale“ diese Kursanomalie zu verantworten hätten. „Wale“. Ich kann dieses Wort schon nicht mehr hören. Eigentlich ist die Bezeichnung „Wale“ für diejenigen mit den größten Bitcoin Beständen eine Beleidigung für die betroffenen Tiere. Wale sind friedliche, sehr soziale und vom Aussterben bedrohte Wesen, welche die Ozeane durchstreifen. Bitcoin Wale sind eher anders zu beschreiben, die Bezeichnung dieser Personen als Theropoden ist weitaus treffender. Theropoden? Sagt Ihnen der Name Tyrannosaurus Rex etwas?
              </p>
              <p>Diese 50 Theropoden sollen also am 27.10.2021 um 06:00 Uhr MEZ eine synchrone Handlung gesetzt, und ca. 800 Millionen Dollar auf ihrem Beutezug verschlungen haben? Könnte sein, ja, denn interessanterweise begannen sämtliche Kryptowerte synchron erheblich an Wert zu verlieren - genau in diesem Zeitraum. Wenn zeitgleich alle Kryptowährungen mindestens 10 Prozent an Wert einbüßen, muss dies auf eine abgekartete Geldbeschaffungsmaßnahme hindeuten. Es kann in China aber auch nur ein Sack Reis umgefallen sein, oder Evergrande musste seine Fahrzeugflotte volltanken. Im Prinzip lassen sich bei genauer Betrachtung unzählige  Kurskorrekturen in den Kursverläufen finden, welche mit Sicherheit ihren Ursprung in einer Marktmanipulation haben. Erfahrene Anleger wissen es längst: es regnet nicht jeden Tag. Schon am nächsten Tag sind die Gelder wieder im Kryptomarkt, sämtliche Kryptos steigen wieder an. Gesundes Wachsen eben, mit attraktiven Einstiegspreisen.
              </p>
                <h4>Fazit</h4>
                <p>Kurschwankungen sind Lebenszeichen und Momentaufnahmen. Nicht mehr, aber auch nicht weniger. Die technische Analyse von Kursverläufen ist Methodik, jedoch keine fundamentale Analyse eines Kryptowertes. Ein Patient kann völlig unauffällige Vitaldaten (Puls, Herzfrequenz, Blutdruck, usw.) aufweisen, und dennoch schwer krank sein. Oder eben sich kerngesund seines Lebens erfreuen. Dasselbe gilt für Kryptos. Nur eine genaue Kenntnis über Technik, Anwendungsgebiete, aber auch Partnerschaften, geben einem Anleger das gute Gefühl, richtig zu liegen. Wir empfehlen Ihnen dazu unsere Rubrik „Rezepte“. 
                </p>
                <br>
                <p>mebe, coinditor 
              </p><!-- ENDE REZEPTE-BEITRAG -->
            </div>
         </div>
    </div>
</div>
<!-- About Section End -->

<?php include 'inc.footer.php'; ?>