<?php include 'inc.header.php'; ?>

<section id="items" >

    <div class="container">
         <div class="heading" data-aos="fade-up" data-aos-delay="300">
            <br>
            <br>
            <br>
            <br>
            <h2>Coin-Rezepte</h2>
            <p>Rezepte sind die Basis für das Verständnis, wie die 
            Krypto-Produkte aufgebaut sind und somit
            der Einstieg in die Krypto-Welt. 
            Erst wenn Sie wissen, wie die hübschen und schmakhaften Coins 
            kredenzt und zusammengesetzt sind, verstehen Sie die Kryptowelt 
            und können richtig damit umgehen.
            Hier vermitteln wir Ihnen die Grundkenntnisse um in die 
            Patisserieprodukte reinzubeissen.</p> 
         </div>

      <div class="row text-center text-lg-left">


        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-left" data-aos-delay="300">
         <div class="imageBox" >
            <a href="#" onclick="window.location.href='rezept-detail-0001.php'" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
              <img class="img-fluid img-thumbnail" src="images/item-1.jpg" alt="">
              <div class="textBox">
                  <h5>HODL</h5>
              </div>
            </a>
         </div>
         <a href="rezept-detail-0001.php" >
           <h6 class="rezepteueberschrift">Buy and Hold Strategie bei Kryptowährungen. Fluch oder Segen?</h6>
         </a>
        </div>
                
        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-left" data-aos-delay="500">
         <div class="imageBox">
          <a href="#" onclick="window.location.href='rezept-detail-0002.php'" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
            <img class="img-fluid img-thumbnail" src="images/item-2.jpg" alt="">
              <div class="textBox">
                  <h5>Bitcoin fällt um 6 Prozent</h5>
              </div>
          </a>
         </div>
         <a href="rezept-detail-0002.php" >
           <h6 class="rezepteueberschrift">Bitcoin fällt um 6 Prozent. Marktmanipulation oder doch nur gesunde Korrektur?
           </h6>
         </a>
        </div>
        
        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-left" data-aos-delay="700">
         <div class="imageBox">
          <a href="#" onclick="window.location.href='rezept-detail-0003.php'" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
            <img class="img-fluid img-thumbnail" src="images/item-3.jpg" alt="">
              <div class="textBox">
                  <h5>Diversifizierung</h5>
              </div>
          </a>
         </div>
        <a href="rezept-detail-0003.php" >
	        <h6 class="rezepteueberschrift">Diversifizierung: Die perfekte Strategie zur Minimierung des Risikos
	        </h6>
        </a>
        </div>
        
        
<!--        
        
        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-left" data-aos-delay="900">
         <div class="imageBox">
          <a href="images/item-4.jpg" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
            <img class="img-fluid img-thumbnail" src="images/item-4.jpg" alt="">
              <div class="textBox">
                  <h5></h5>
              </div>
          </a>
         </div>
        <a href="rezept-detail-0000.php" >
	        <h6 class="rezepteueberschrift"></h6>
        </a>
        </div>





        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-left" data-aos-delay="300">
         <div class="imageBox" >
            <a href="rezept-detail-0000.php" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
              <img class="img-fluid img-thumbnail" src="images/item-1.jpg" alt="">
              <div class="textBox">
                  <h5>Torten</h5>
              </div>
            </a>
         </div>
        <a href="rezept-detail-0000.php" >
          <h6 class="rezepteueberschrift">Wie sieht der Krypto-Kuchen aus?</h6>
        </a>
        </div>
                
        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-left" data-aos-delay="500">
         <div class="imageBox">
          <a href="images/item-2.jpg" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
            <img class="img-fluid img-thumbnail" src="images/item-2.jpg" alt="">
              <div class="textBox">
                  <h5>Glasierte Nüsse</h5>
              </div>
          </a>
         </div>
        <a href="rezept-detail-0000.php" >
          <h6 class="rezepteueberschrift">Wieviele unterschiedliche Coins gibt es?</h6>
        </a>
        </div>
        
        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-left" data-aos-delay="700">
         <div class="imageBox">
          <a href="images/item-3.jpg" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
            <img class="img-fluid img-thumbnail" src="images/item-3.jpg" alt="">
              <div class="textBox">
                  <h5>Crème-Törtchen</h5>
              </div>
          </a>
         </div>
        <a href="rezept-detail-0000.php" >
	        <h6 class="rezepteueberschrift">Wie kann ich mit Coins Geld verdienen?</h6>
        </a>
        </div>
        
        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-left" data-aos-delay="900">
         <div class="imageBox">
          <a href="images/item-4.jpg" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
            <img class="img-fluid img-thumbnail" src="images/item-4.jpg" alt="">
              <div class="textBox">
                  <h5>Nougats</h5>
              </div>
          </a>
         </div>
         <a href="rezept-detail-0000.php" >
	         <h6 class="rezepteueberschrift">Sind Kryptowährungen auch Währungen?</h6>
         </a>
        </div>

        


        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-right" data-aos-delay="300">
         <div class="imageBox">
          <a href="images/item-5.jpg" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
            <img class="img-fluid img-thumbnail" src="images/item-5.jpg" alt="">
              <div class="textBox">
                  <h5>Choco-Pralinés</h5>
              </div>
          </a>
         </div>
         <a href="rezept-detail-0000.php" >
	         <h6 class="rezepteueberschrift">Welche Krypto-Coins sind relevant?</h6>
	       </a>
        </div>
        
        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-right" data-aos-delay="500">
         <div class="imageBox">
          <a href="images/item-6.jpg" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
            <img class="img-fluid img-thumbnail" src="images/item-6.jpg" alt="">
              <div class="textBox">
                  <h5>Macarons</h5>
              </div>
          </a>
         </div>
         <a href="rezept-detail-0000.php" >
	         <h6 class="rezepteueberschrift">Auf welchen Plattformen gibt es Kryptos?</h6>
	       </a>
        </div>
        
        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-right" data-aos-delay="700">
         <div class="imageBox">
          <a href="images/item-7.jpg" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
            <img class="img-fluid img-thumbnail" src="images/item-7.jpg" alt="">
              <div class="textBox">
                  <h5>Mandarin-Crèmes</h5>
              </div>
          </a>
         </div>
         <a href="rezept-detail-0000.php" >
	         <h6 class="rezepteueberschrift">Was ist die Funktion einer Währung?</h6>
	       </a>
        </div>
        
        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-right" data-aos-delay="900">
         <div class="imageBox">
          <a href="images/item-8.jpg" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
            <img class="img-fluid img-thumbnail" src="images/item-8.jpg" alt="">
              <div class="textBox">
                  <h5>Himbeer Eclaires</h5>
              </div>
          </a>
         </div>
         <a href="rezept-detail-0000.php" >
    	     <h6 class="rezepteueberschrift">Was sind die Eclairs der Kryptowelt?</h6>
    	   </a>
        </div>
        
        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-left" data-aos-delay="300">
         <div class="imageBox">
          <a href="images/item-11.jpg" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
            <img class="img-fluid img-thumbnail" src="images/item-11.jpg" alt="">
              <div class="textBox">
                  <h5>Patisserie</h5>
              </div>
          </a>
         </div>
         <a href="rezept-detail-0000.php" >
	        <h6 class="rezepteueberschrift">Wie gross und wichtig ist die Kryptowelt?</h6>
	       </a>
        </div>
        
        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-left" data-aos-delay="500">
         <div class="imageBox">
          <a href="images/blog-1.jpg" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
            <img class="img-fluid img-thumbnail" src="images/blog-1.jpg" alt="">
              <div class="textBox">
                  <h5>Appetizers</h5>
              </div>
          </a>
         </div>
         <a href="rezept-detail-0000.php" >
	         <h6 class="rezepteueberschrift">Welche Coins sind die Höhenflieger?</h6>
	       </a>
        </div>
        
        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-left" data-aos-delay="700">
         <div class="imageBox">
          <a href="images/item-12.jpg" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
            <img class="img-fluid img-thumbnail" src="images/item-12.jpg" alt="">
              <div class="textBox">
                  <h5>Fruchttörtchen</h5>
              </div>
          </a>
         </div>
         <a href="rezept-detail-0000.php" >
	         <h6 class="rezepteueberschrift">Wie beobachtet man den Krypto-Markt?</h6>
	       </a>
        </div>
        
        <div class="col-lg-3 col-md-4 col-6" data-aos="fade-left" data-aos-delay="900">
         <div class="imageBox">
          <a href="images/item-9.jpg" class="d-block mb-4 h-100" data-toggle="lightbox" data-gallery="example-gallery">
            <img class="img-fluid img-thumbnail" src="images/item-9.jpg" alt="">
              <div class="textBox">
                  <h5>White-glosses</h5>
              </div>
          </a>
         </div>
         <a href="rezept-detail-0000.php" >
	         <h6 class="rezepteueberschrift">Wie findet man die glänzenden Coins?</h6>
         </a>
        </div>
-->
      </div>

    </div>
    <!-- /.container -->
</section>
<!-- Items section Ended-->

<?php //include 'inc.subscribe.php' ?>

<?php include 'inc.footer.php' ?>

<script>
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
</script>
