<?php include_once 'inc.header.php'; ?>

<div id="rezeptedetail">
    <div class="container">
       <h3>Buy and Hold Strategie bei Kryptowährungen. Fluch oder Segen?
       </h3>
       <a class="btn btn-success" href="rezepte.php" role="button" data-aos="fade-up" data-aos-delay="1000">zurück zu den Coin-Rezepte</a>
       <br><br>
        <div class="row" style="margin-right: 0; margin-left: 0;">
            <div data-aos="fade-up" data-aos-duration="800">
             <img src="images/item-1.jpg" alt="about-bg" class="thumbnail image">
              <p><!-- START REZEPTE-BEITRAG -->

              Sammler von Kunstwerken, oder auch Weinkenner würden sofort zustimmen: kaufen, behalten und sich so lange wie möglich am Objekt der Begierde erfreuen, das ist ihr Dogma. Das Kunstwerk könnte im Laufe der Zeit erheblich an Wert gewinnen, die Flasche Wein bei richtiger Lagerung noch besser und teurer werden. Im idealen Fall stehen somit in ferner Zukunft erhebliche Vermögenswerte im speziell ausgestatten Lagerraum. Ob diese Werte jemals realisiert werden, und sich der Sammler von seinen Schätzen trennen wird? Wohl eher nicht, denn das Ganze ist inzwischen eine Wertanlage und könnte ja noch an Wert zulegen.
              <br><br>
              Der erfahrende Börsianer, der über die letzten 40 Jahre mit Aktien und Anleihen seine ganz speziellen Erfahrungen gemacht hat, würde eher nicht zustimmen, dass die Buy and Hold Strategie zwingend einen garantierten Vermögenszuwachs bedeutet. Zu tief sitzen seine prägenden Erinnerungen an richtig und lang andauernde „schlechte Zeiten“: kleine und große Börsencrashs wie 1987, 1997, 2000 Dotcom Blase, und zuletzt 2008 die Hypothekenkrise. Und momentan könnte die „China - Connection“ mit  „Evergrande“ noch viel mehr Ungemach bedeuten. Die Börse ist eines gewohnt: Krise, Krise, Krise. Unnötig zu erwähnen, dass diese ständigen Stresstests die Börsen nicht etwa resistenter gemacht haben - sicher nicht. Aber es ist festzustellen, dass „kleine Krisen“ nicht mehr sofort eine globale Kettenreaktion auslösen, und sich die Kurse danach schneller erholen. Eines kann die klassische Börse jedoch nicht: kurzfristig Zuwachsraten wie bei Kryptowährungen bieten.
              <br><br>
              Schenkt man Blockchain Datenanalysen Glauben, ist der typische Krypto - Anleger männlich, und zwischen 17 und 30 Jahren alt. Die meisten Krypto User leben in Indien. Von Krypto Crashs ist alle 2 Monate in den Medien zu hören, aber - um ehrlich zu sein - mit richtig harten Durststrecken sah sich der recht junge Krypto Markt bisher noch nie konfrontiert. Bitcoin startete 2009, damals mit einem Wert von 0,08 Cent pro Coin. Anekdote am Rande:  wenn Sie 2010 für 40 Dollar 160 Bitcoins gekauft hätten, könnten Sie sich aktuell (Stand: 21. Oktober 2021) an einem Vermögen von 10,5 Millionen US-Dollar erfreuen. So viel zum erlauchten Kreis von Bitcoin Millionären, welche ihr Vermögen „ausschließlich ihrem messerscharfen Verstand, und ihrem einzigartigen Investment Geschick (oder Glück?) zu verdanken haben“. Die mittlere Haltefrist einer Kryptowährung wird auf Krypto Börsen recht transparent ausgewiesen - sie liegt zwischen wenigen Sekunden und mehreren Monaten. Im Gegensatz zu Aktienbörsen, auf welchen nur zu bestimmten Börsenzeiten gehandelt werden kann, ist der Kryptomarkt 24/7/365 aktiv. Durch die ungeheure Vielfalt an Zugangsmöglichkeiten zu jeglichen Kryptobörsen auf der ganzen Welt ist der Handel mit digitalen Vermögenswerten (Coins, Tokens, NFTs) fast unbegrenzt möglich - außer China grätscht mal wieder dazwischen. Dennoch ist auch bei Kryptowährungen ein Trend festzustellen: gerade die Anzahl an Bitcoin Haltern, welche ihre Vermögenswerte richtiggehend bunkern, ist in den letzten Wochen stark ansteigend. Buy and Hold also auch hier - trotz atemberaubender Volatilität?

              </p>
              <h4>Die Frage, ob Buy and Hold, auch „HODL“ genannt, </h4>
              <p>
              DIE Anlagestrategie für alle Kryptos schlechthin darstellt, kann seriös nicht mit Ja, bzw. Nein beantwortet werden. Die Frage muss zwingend umformuliert werden, um einer Fehlinvestition samt den damit verbundenen Handlungsirrtümern vorzubeugen! 
                Damit Buy and Hold gelingen kann, müssen zuerst <b>die Rahmenbedingungen</b> analysiert werden, welche einem erfolgreichen „HODL“ zugrunde liegen.
                <ul>
                    <li>Bedingung 1: der Risikokapital ist vorhanden
                    </li>    
                    <li>Bedingung 2: sie wissen ganz genau, in welchen Titel / Coin / Token sie investieren
                    </li>    
                    <li>Bedingung 3: der Anlagezeitraum wurde exakt definiert
                    </li>    
                    <li>Bedingung 4: zusätzliche Liquidität zur Reserve ist ggf. rasch verfügbar
                    </li>    
                    <li>Bedingung 5: der Markt wird genau beobachtet und die Strategie kann anpasst werden
                    </li>    
                    <li>Bedingung 6: ihr Nervenkostüm ist passend und sitzt
                    </li>   
                </ul>
              </p>
              <h4>Risikokapital</h4>
              <p>Den Begriff "Risikokapital" verwendet die Coinditorei hier ganz bewusst, denn Investments im Kryptobereich weisen ein erhebliches Risiko auf, und im schlimmsten Fall droht der Totalverlust. Die Gründe für schmerzhafte Verluste können sehr unterschiedlich sein: eine falsche Strategie, oder einfach nur aufs falsche Pferd gesetzt, einem Scam auf den Leim gegangen, oder einfach nur zur falschen Zeit auf den Wundercoin gehofft, und schon ist der Zauber möglicherweise für längere Zeit vorbei. Von Defekten an Endgeräten (PC, Handy, usw.) oder Softwarepannen abgesehen, stellen diese Szenarien den Worst Case dar.
              </p>
              <p>Ein seriöser Experte wird Ihnen stets denselben Ratschlag erteilen: „Investieren Sie nur Geld, welches sie bereit sind, vollständig zu verlieren...“ Zitat Ende. Klingt furchteinflößend, sorgt jedoch auch gleichzeitig für ernüchternde Gewissheit. Doch was bedeuten gutgemeinte Ratschläge für potenzielle InvestorInnen in der Praxis? Ganz einfach und ohne Lametta:
              </p>
              <ul>
                <li>nehmen Sie nur Geld für ihr Vorhaben, welches Sie nicht für ihren Lebensunterhalt benötigen
                </li>
                <li>investieren Sie nur dann, wenn Sie sich nicht zu sehr einschränken müssen
                </li>
                <li>verschulden Sie sich niemals vorab für ihr Investment 
                </li>
                <li>planen Sie niemals ein allzu attraktives Zukunftsbild vom erwarteten Vermögenszugewinn ein
                </li>
                <li>bleiben Sie realistisch, und koppeln Sie sich vom Hype ab
                </li>
                <li>wenn Sie einen emotionalen Spagat machen müssen, und ihr Rückgrat zu sehr spüren - Finger weg
                </li>
                <li>bei Buy and Hold muss das Kapital für das Investment schon bereit stehen. Wenn das Kapital erst durch kurzfristige Kursgewinne erwirtschaftet werden muss, wählen Sie besser eine andere Anlagestrategie
                </li>
              </ul>
              <p>Ob Sie nun beim Buy and Hold „All-in“ gehen, oder mit 60 Prozent ihres verfügbaren Budgets einsteigen - es ist und bleibt ihre Entscheidung. Bedenken Sie jedoch stets: wenn Sie in Kryptowährungen investieren wollen, sind Sie zugleich auch ihre eigene Bank. Für sämtliche Transaktionen, und alle nachfolgenden Aktionen auf der gewählten Handelsplattform sind Sie selbst verantwortlich. Wenn etwas schief geht, ist es nicht mit einem Achselzucken, einem Telefonat, oder Mail bereinigt. Teilweise sind Millionen von Kunden an den Kryptobörsen registriert, also erheblich mehr Kunden als bei ihrer Hausbank, und Kundenservice müssen diese Plattformen erst erlernen. Service kostet, und diese Kosten wollen Kryptobörsen um jeden Preis vermeiden.
              </p>
            
              <h4>Titel / Coin / Token, bzw. Portfolio</h4>
            <p>Sie kennen den Vermögenswert ganz genau, in den Sie investieren wollen? Sie haben entsprechende Recherchen durchgeführt, die Basisanwendungen verstanden, die Charts analysiert, und sind sich ganz sicher, dass es ein gutes Investment darstellt? Auch der Zeitpunkt könnte nicht besser passen, um mit Buy and Hold einzusteigen? Gratulation, dann haben sie mehr Aufwand betrieben als 99 Prozent der übrigen Anwender. Oder sie haben die Beiträge der <b>Coinditorei</b> verfolgt, und das virtuelle Portfolio aus unserem Mitgliederbereich nachgebildet. 
            </p>
            <p>Dennoch. Auch wenn der ersehnte Coin noch so schön funkelt - bedenken Sie die Lebensdauer eines Vermögenswertes. Egal ob Aktie, oder Krypto - stellen sie sich vor allem bei Buy and Hold die Frage, wie lange die Halbwertszeit des Titels wohl sein wird. Kryptowährungen sind schnelllebiger als Eintagsfliegen. Ab welchem Zeitpunkt ist ihr Coin technisch überholt, bzw. wird er von einem anderen Protokoll ersetzt? Rhetorische Frage: akzeptieren Sie die Tatsache, dass auch die heutigen Tech Giganten nur noch deswegen existieren, weil sie noch nicht von anderen Konzernen abgelöst wurden? Im Haifischbecken der Finanzwelt gibt es immer einen noch größeren Räuber- oder denken Sie, dass es z.B. Amazon, oder auch Tesla, in 5 bis 10 Jahren noch immer in dieser Form geben wird?
            </p>
            <h4>Anlagezeitraum</h4>
            <p>Der Anlagehorizont erweist sich bei der Buy and Hold Strategie als ganz entscheidendes Kriterium. Und hier müssen angehende Investoren sehr ernst und gewissenhaft zu sich selbst sein.
            </p>
            <p>Junge Zeitgenossen können leichten Herzens einen Anlagezeitraum von 15- 30 Jahren einplanen. 
            </p>
            <p>Bei reiferen Semestern ist hingegen möglicherweise ein Anlagezeitraum von maximal 20 Jahren sinnvoll. Dies hat nichts mit der Lebenserwartung zu tun, sondern mit einem viel naheliegenderen Grund: geistig fit zu sein, und auch zu bleiben, ist eine Grundvoraussetzung für ein selbstverwaltetes Investment.
            </p>
            <p>Risikoarme Lebensführung, sportlicher Lebensstil, gesunde Ernährung, beste medizinische Versorgung, und überall das doppelte Netz. Die Lebenserwartung steigt ja sowieso, also wo liegt das Problem? Oft heißt die Antwort schlicht und ergreifend „Schicksal“, denn vor Unfällen, schweren Erkrankungen, Herzinfarkt oder Schlaganfall, kann sie ihr Optimismus nicht schützen. Können Sie garantieren, mit über 70 Jahren noch selbstständig und gesund genug zu sein? Was, wenn Sie dann mit datumwörtern nicht mehr zurecht kommen, oder nicht mehr mit dem technischen Fortschritt mithalten können, bzw. wollen? Benötigen Sie jetzt bereits Hilfe beim Bedienen ihres Handys oder Computers? Sie wären nicht die erste Person, welche ein Vermögen an Kryptowährungen unbemerkt im Dschungel des Kryptomarktes zurück lässt. Haben sie irgendwo schriftlich ihre Vermögenswerte festgehalten, und eine Vertrauensperson darüber informiert? Keine Kryptobörse, in Asien oder den USA, wird sich bei ihren Erben von sich aus melden. Weil sie nur eine Kundennummer sind und auch bleiben. Ihre Hausbank wird hingegen sehr rasch reagieren.
            </p>

            <h4>Zusätzliche Liquidität/ Reserven</h4>
            <p>Bei Buy and Hold über einen langen Zeitraum ist es unabdinglich, zusätzliche Liquidität im Bedarfsfall bereitstellen zu können. Kombiniert mit der Strategie, bei fallenden Kursen nachzukaufen, wird weiteres Kapital eingebracht, um einen Durchschnittskosteneffekt zu erzielen. Es könnte ja durchaus zu Beginn ihres Investments der Fall gewesen sein, dass der Einstieg zu einem ungünstig hohen Preisniveau erfolgte. Welche Summen Sie hier genau einplanen sollten, ist recht einfach zu beantworten. Betrachten Sie ihr Investment als „heißen Stein“. Ein Tropfen verdampft sofort, nur ein Wasserstrahl erzielt einen Effekt. In Zahlen? Wenn sie 100 Tausend investiert haben, erzeugen weitere 1000 nur einen Hauch von Dampf...
            </p>
            <p>Was jedoch, wenn Sie kein Kapital mehr nachlegen können? Aus welchen Gründen auch immer. Sie wissen mit Sicherheit zum Zeitpunkt des ursprünglichen Investments, ja eher selten, wie sich ihre Lebenssituation in den nächsten 10 Jahren gestalten wird. Die einzige Konstante ist die Veränderung. So viel ist sicher. Veränderungen beruflicher Natur sind zumeist unvorhersehbar, persönliche Krisensituationen noch weniger. Auch die eigene Motivation spielt eine Rolle, denn Sie sollten ihr Investment keinesfalls aufgrund von temporärem Desinteresse aufs Spiel setzen. Der Einsatz ist einfach zu hoch! Bedenken Sie deshalb schon von Anfang an, ob Sie alles sofort investieren wollen, oder ein Nachkauf mit Reserven nicht doch von Vorteil sein kann. 
            </p>

            <h4>Markt beobachten und Strategie anpassen</h4>
            <p>Bei Buy and Hold bitte nicht darauf verlassen, dass schon alles gut gehen wird! Die beständige Beobachtung des Marktes ist genauso unverzichtbar, wie die Bereitschaft, bei fundamentalen Veränderungen des Marktes entsprechend zeitnah zu reagieren. Beim Nachkauf aufgrund günstiger Gelegenheiten sollten die Entscheidungen genauso sitzen, wie im Falle eines drohenden Totalverlustes. Dies nennt sich „aktives Investment“ und bedeutet im praktischen Kontext, Titel auch kurzfristig auszutauschen, ggf. auch zu liquidieren. Nur weil der Vermögenswert kurzfristig verrücktspielt, und die Daten des Kursverlaufes den Vitalparameter eines Intensivpatienten ähnlich sehen, so ist dies noch lange kein Grund zur Besorgnis. Es müssen schon andere Gründe sein, weshalb dem Vermögenswert bei Buy and Hold ein gesteigertes Maß an Aufmerksamkeit zuteilwird. Gerade Kryptos, welche im Finanzsektor beheimatet sind, können technisch aus heiterem Himmel überholt sein. Und plötzlich ist alles anders. Erwähnt sei an dieser Stelle, dass sich regulatorische Probleme einer Kryptowährung nicht über Nacht ergeben und Nachrichten über spezifische Probleme in den sozialen Netzwerken sehr schnell viral gehen. Die Devise lautet daher: gerade bei Buy and Hold sollten ihre Ohren und Augen überall sein. Wenn auch mit der gebotenen Gelassenheit. Und da kommen wir schon zum letzten Thema...
            </p>

            <h4>Das passende Nervenkostüm</h4>
            <p>Angesichts der atemberaubenden Zuwachsraten mancher Kryptowerte, welche rein gar keinen praktischen Anwendungsfall abbilden, kann mit Fug und Recht behauptet werden, dass der Kryptomarkt sehr stark von Emotionen, Sentiments, und Moods geprägt ist. Zwischen Gier frisst Hirn, FOMO (Fear of missing out- die Angst etwas zu versäumen) und manipulierendem Gezwitscher eines exzentrischen Milliardärs aus den USA, müssen wir kleinen Privatpersonen unseren Platz behaupten. Sich dabei nicht vom Hype anstecken zu lassen, oder auf die Versprechungen großmäuliger Influencer „sozialer Plattformen“ nicht hereinzufallen, ist und bleibt eine Geduldsprobe. Bei Buy and Hold kommt noch eine weitere Komponente hinzu. Was tun, wenn langwährende Abwärtsphasen, oder nur noch zuckende Seitwärtsbewegungen den Kursverlauf dominieren? Bringen Sie dann noch den Mut auf, in den Kryptomarkt erneut zu investieren? Sind sie fähig, sich so weit abzugrenzen, dass sie die nächsten Jahre nicht bei jeder Gelegenheit den Kursverlauf ihres Investments verfolgen müssen? Können sie großmütig über die ständigen Marktmanipulationen hinwegsehen? Diese allzu psychologischen und zutiefst menschlichen Entscheidungen kann ihnen nicht mal die beste und sicherste Investmentstrategie abnehmen. Übrigens: ein wirklich erfolgreicher Kryptoinvestor ist stets zurückhaltend, gelassen, und still, wenn das Thema Kryptowährungen Erwähnung findet. Ein erfahrener Jäger, oder Fischer, wird niemals sein bestes Revier preisgeben, und schon gar nicht seine Beute lautstark und publikumswirksam präsentieren.  
            </p>
            <br><br>
            

            <h4>Fazit zu Buy and Hold</h4>
            <p>Egal, ob wir jetzt zur Beantwortung der Frage die Wertentwicklung von Aktien oder Kryptowährungen insgesamt berücksichtigen: über einen sehr langen Zeitraum betrachtet, fällt die Antwort leicht. Praktisch jeder Chart wird ein beständiges Wachstum zeigen. Betrachtet man die Charts jedoch in bestimmten Zeiteinheiten, fällt auf, dass es vom Zeitpunkt des Einstieges abhängt, ob Buy and Hold sinnvoll ist, und ab wann Gewinne realisiert werden könnten. Möglicherweise hat die Investition ja im ungünstigsten Zeitpunkt stattgefunden, und z.B nach einem Ereignis (Blase, Finanzkrise) würde  der Kurs mehrere Jahre benötigen, um sich zu erholen. Bei Aktien sind diese Zeiträume jedenfalls deutlich länger ausgeprägt als bei Kryptowährungen. Die hohe Volatilität sorgt bei Kryptowährungen in fast schon regelmäßigen Zeiträumen zu enormen Zuwachsraten, aber eben auch zu erheblichen Kurseinbrüchen. 
            </p>
            <p>All diese Betrachtungen müssen zur Erkenntnis führen, dass Buy and Hold bei Kryptowährungen nur dann nachhaltig funktionieren kann, wenn eine gesunde Mischung an Kryptowährungen in einem sinnvollen Verhältnis zueinander in einem Portfolio Verwendung finden. Dies nennt sich auch Diversifikation. 
            </p>
            <p>Die besten Analysten können unmöglich wissen, was die nächsten 2 Monate bringen werden, und noch weniger, was in fünf Jahren passieren wird. Sein gesamtes Risikokapital mit der Hoffnung auf Wunder und Wahrscheinlichkeit in einen einzelnen Vermögenswert zu stecken, geht weit über die Grenzen von Wagemut hinaus.
            </p>
            <p>Mahnende Worte zum Abschluss: die Lebensdauer einer Aktie aus dem Bereich S&P 500 lag früher bei 60, heute bei 20 Jahren. Momentan sind auf Coinmarketcap über 13000 Kryptos gelistet, und täglich werden es Hunderte mehr. Es ist davon auszugehen, dass 95 Prozent der aktuellen Kryptowährungen nicht überleben werden...
            </p>
            <p>Es gilt somit umso mehr das alte Sprichwort: „Drum prüfe, wer sich ewig bindet“. Im Mitgliederbereich finden sie virtuelle Portfolios für die nächsten Jahrzehnte, genug für ein ganzes süßes Leben lang.
            </p>
            <br>
            <p>mebe, Coinditor
              </p><!-- ENDE REZEPTE-BEITRAG -->
            </div>
         </div>
    </div>
</div>
<!-- About Section End -->

<?php include 'inc.footer.php'; ?>