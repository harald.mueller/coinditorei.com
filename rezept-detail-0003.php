<?php include 'inc.header.php'; ?>

<div id="rezeptedetail">
    <div class="container">
       <h3>Diversifizierung: Die perfekte Strategie zur Minimierung des Risikos
       </h3>
       <a class="btn btn-success" href="rezepte.php" role="button" data-aos="fade-up" data-aos-delay="1000">zurück zu den Coin-Rezepte</a>
        <br><br>
        <div class="row" style="margin-right: 0; margin-left: 0;">
            <div data-aos="fade-up" data-aos-duration="800">
             <img src="images/item-3.jpg" alt="about-bg" class="thumbnail image">
              <p><!-- START REZEPTE-BEITRAG -->
[1.11.2021]
</p>
<p class="zitat">
„Mein Portfolio macht mir Sorgen, es zieht nicht...“.
</p>
<p class="zitat">
„Wie, es zieht nicht?“.
</p>
<p class="zitat">
„Naja, irgendwie bewegt es sich nicht so, wie ich es gerne hätte ...“. 
</p>
<p class="zitat">
„Welche Titel sind denn im Portfolio?“.
</p>
<p class="zitat">
„Ich glaube, es sind 6: Bitcoin, Cardano, Ethereum, Vechain, Ripple, und, ...hm..., ach ja Link.“
</p>
<p class="zitat">
„Alles Top Titel, die du dir da ausgesucht hast. Wo ist das Problem?“
</p>
<p class="zitat">
„Wenn der Bitcoin fällt, dann fallen die anderen Titel um das Doppelte. Danach brauchen die anderen Titel etliche Wochen, um wieder denselben Wert wie vorher zu haben. Das nervt mich. Ich werde es mal mit Shiba probieren, der soll gut laufen...“
</p>
<p class="zitat">
„Hast du dich mal mit Exchange Tokens beschäftigt? Schau dir doch mal WazirX an.“
</p>
<p class="zitat">
„ ... du meinst die NFT oder so? Ja klar, aber die bringen nichts. Ich glaube, ich lass es, und warte ab.“
</p>
<p>
Smalltalk wie dieser ist und bleibt unvermeidlich, wenn es um das Thema Kryptowährung geht. Meist entwickelt sich solch ein Gespräche aus heiterem Himmel, ohne großen Anlass. Irgendwie könnte es auch das Thema Wetter sein, das Resultat bliebe dasselbe. Manche Menschen wollen aber auch ganz einfach nur reden, die Antwort interessiert sie nicht. Aus diesem Grund versuche ich niemals, Menschen von etwas zu überzeugen. Dennoch, manche meiner „Hinweise“ verbleiben im Gehirn meiner Gesprächspartner, irgendwo zwischen frontalem Kortex und Jammerlappen. Denn ein paar Tage später kontaktierte mich der betreffende Gesprächspartner, und fragte nach Exchange Tokens. Interessant. Ich verwies ihn auf den Mitgliederbereich der Coinditorei, denn allein die Information zu Exchange Tokens hätte ihm nicht weitergeholfen. 
</p>

<h4>
Das Zauberwort lautet Diversifizierung
</h4>

<p>
Diversifizierung bezeichnet eine Strategie zur Minimierung des Risikos durch Kombination von Vermögenswerten, welche jedoch unterschiedlich auf ähnlich gelagerte Risiken reagieren. Das klassische Börsen Portfolio beinhaltet z.B. neben Aktien und Goldanteilen auch Anleihen. Dies alles wird in einem zweckmäßigen Anteil zueinander gewichtet (z.B. früher 60:40), fertig ist ein solid diversifiziertes Portfolio. Das einzige Ziel dieser Aktion ist es, sich nicht vom einzelnen Schaukampf am Markt in den Abgrund ziehen zu lassen. Erinnern sie sich an das Platzen der Hypothekenblase in den USA im Jahr 2007 und 2008? Ein Portfolio, welches damals ausschließlich mit Finanztiteln bestückt war, bescherte den Investoren ein handfestes Waterloo. Dasselbe ist aktuell aufgrund der Lieferengpässe wegen dem Corona Theater zu beobachten. Dieses Waterloo wenden diesmal jedoch die Steuerzahler durch ihre großzügigen Krisenhilfen gerade noch ab (Achtung Ironie).
</p>
<p>
Klar ist aber auch, dass nicht jeder Titel gleich stark an Wert zulegen wird, im Durchschnitt erhofft sich der Anleger jedoch eine positive Entwicklung.
</p>

<h4>
Diversifizierung von Krypto Portfolios
</h4>

<p>
Tja, was ist zu tun im Kryptomarkt? Gold gibt es (noch) nicht als Vermögenswert, Aktien (hm, da war mal was), und Anliehen (Shares)? Welche Möglichkeiten einer Diversifizierung bietet denn der Kryptomarkt? Sind doch alles nur Softwareprogramme, welche irgendwie immer mit Bitcoin zusammenhängen? Nichts Greifbares, nichts Physisches. Kryptos kann man nicht angreifen! Wie soll man das diversifizieren? Es gibt Hoffnung, wenn unter anderem folgende Kriterien Beachtung finden:
</p>

<h5>
Kriterium 1: Auswahl der einzelnen Kryptos
</h5>
<p>
Krypto ist nicht gleich Krypto. Bitcoin ist ein Zahlungsmittel, also ein „Zahlungsmittel Krypto“, ebenso wie z.B. der Litecoin. Ethereum ist ein Plattformen Kryptowert, ebenso Cardano. Sandbox und Decentraland sind Kryptowerte des Entertainment/ Gaming Bereiches. Dem Bankensektor zuzuordnen sind z.B.  Stellar Lumens und Ripple. Sie bemerken, wohin die Reise geht? Die Typisierung der Kryptowerte und die damit zwingend benötigten Kenntnisse zum Anwendungsfall sind entscheidende Kriterien, inwieweit von einer Diversifizierung überhaupt gesprochen werden kann. Ganz klar: für manche Kryptowerte ist der ideale Zeitpunkt und Anwendungsfall noch keinesfalls gekommen. IOT (Internet of Things) scharrt in den Startlöchern, jedoch erweist sich die Vernetzung von physischen und virtuellen Objekten als durchaus anspruchsvolles Unterfangen. Meta wird hier recht zeitnah für Lösungen sorgen, denn wer sonst könnte so gewinnbringend sämtliche Informations- und Kommunikationstechniken zusammenbringen.
</p>

<h5>
Kriterium 2: Wichtung von Kryptowährungen 
</h5>
<p>
90 Prozent Bitcoin, 5 Prozent Ethereum, 3 Prozent Cardano, den Rest auf Wald und Wiese aufgeteilt? Kann man, muss man aber nicht. Wie wäre es mit einer etwas mutigeren Aufteilung? Bitcoin ist eine sichere Bank, wird jedoch ungeheure Geldmittel benötigen, um die Marktkapitalisierung und den Kurs weiter in die Höhe zu bringen. Ein innovativer Kryptowert aus der Liste nach Marktkapitalisierung zwischen Platz 80 und 200 könnte mit viel geringeren Mitteln, und aus heiterem Himmel plötzlich seinen Wert vervielfachen. Klarer Fall: sicher ist sicher, aber eben auch stinklangweilig. Weshalb Zeit und Geld mit Bitcoin vergeuden, wenn es noch Hunderte viel interessantere Kryptos da draußen gibt? Am Ende wird niemanden interessieren, womit sie ihr Vermögen gemacht haben. Gerade die Schulterklopfer sind allesamt Feiglinge! Wichten sie daher progressiv und dynamisch, nehmen Sie Gewinne mit und konvertieren sie diese Gewinne innerhalb des Portfolios gerade so, wie sich die Marktlage verändert. Sie hören, dass Ripple der SEC den Marsch bläst, und XRP auch wieder auf Coinbase gelistet wird? Nicht nur Bart Simpson wird sich freuen!
</p>

<h5>
Kriterium 3: Kryptos verteilen  
</h5>
<p>
Nein, nicht verschenken ist damit gemeint. Diversifikation wird oft nur sehr einseitig verstanden. Da es unmittelbar um Minimierung von Risiken geht, sollte bei der Diversifizierung nicht auf die alltäglichen Herausforderungen vergessen werden. Was nutzt das smartest diversifizierte Portfolio der Welt denn, wenn sie z.B. gehackt werden? Alles weg? Oder Handy verloren? Ihnen passiert sowas nicht? Na dann viel Spaß. Im praktischen Anwendungsfall bedeutet schon der Verlust der Zugangsdaten durch Probleme mit der Hardware, dass sie bei der Handelsplattform mehrere Tage abwarten müssen, um wieder freigeschaltet zu werden. In dieser Zeit gehen garantiert die Kurse durch die Decke, und sie müssen zusehen, wie die ganze Mühe umsonst war. Es empfiehlt sich daher, auf mehreren Handelsplattformen tätig zu sein. Im Notfall wird schon irgendeine Plattform noch funktionieren, auch wenn die Chat Welt stillsteht, weil in Mountain View wieder irgendein Spaßvogel im Serverraum etwas verbockt hat (wird zumindest so kommuniziert). Angenehmer Nebeneffekt: die mitunter saftigen Gebühren für das Handeln sind von der Handelsplattform abhängig. Alles, was sich sparen lässt, vermehrt ihren Gewinn. 
</p>

<h4>
Fazit
</h4>
<p>
Auf die richtige Mischung kommt es an. Und: eine Strategie ist immer besser als eine Meinung. Wenn Sie mehr über die praktische Umsetzung der Diversifizierung eines Krypto Portfolios lernen wollen, werden sie Mitglied bei der Coinditorei, und profitieren sofort von neuen Handlungskompetenzen.
</p>

<p>  
mebe, Coinditor            
              </p><!-- ENDE REZEPTE-BEITRAG -->
            </div>
         </div>
    </div>
</div>
<!-- About Section End -->

<?php include 'inc.footer.php'; ?>