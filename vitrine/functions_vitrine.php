<?php 
if (!isset($_SESSION)) { 
    session_start(); 
}
$_SESSION['fileVitrinenListe'] = "../data/vitrine.json";

//echo "<pre>REQUEST: ".print_r($_REQUEST, TRUE)."</pre>";// zu Testzwecken
//echo "<pre>SESSION: ".print_r($_SESSION, TRUE)."</pre>";// zu Testzwecken


if (isset($_REQUEST["btn_cancel"])) {
    unset($_SESSION["inputDetailFelder"]);
    header("Location: ./");
}

if (isset($_REQUEST["editRow"])) {
    $_SESSION["inputDetailFelder"] = TRUE;
    $_SESSION["editRow"] = $_REQUEST["editRow"];
//     $_SESSION["keepoff"] = $_REQUEST["keepoff"];
//     $_SESSION["keepon"] = $_REQUEST["keepon"];
//     $_SESSION["name"] = $_REQUEST["name"];
//     $_SESSION["kateg"] = $_REQUEST["kateg"];
//     $_SESSION["preiszielUSD"] = $_REQUEST["preiszielUSD"];

    header("Location: ./");
    //header("Location: ../vitrinenDetailFelder.php"); 
}


if (isset($_REQUEST["addRow"])) {
    $_SESSION["inputDetailFelder"] = TRUE;
    header("Location: ./");
    //header("Location: ../vitrinenDetailFelder.php");
}

if (isset($_REQUEST["btn_addSave"])) {
    unset($_SESSION["inputDetailFelder"]);
    $fileName = "".$_SESSION['fileVitrinenListe'];
    $vitrinenListe = array();
    if (file_exists($fileName)) {
        $json_data = file_get_contents($fileName);
        $vitrinenCoinsArray = json_decode($json_data, true);
        
        $key = htmlentities($_REQUEST["hiddenKey"]);// bei disabled im edit kommt Wert nicht mit 
        if (strlen($key) < 3) {//wenn neu
            $key = strtoupper( htmlentities($_REQUEST["key"]) );
            if (strlen($key) < 3) { //falls inkorrekt
                $key = "XXX".$key;
            }
        }
        $name = htmlentities($_REQUEST["name"]); 
        $kateg = htmlentities($_REQUEST["kateg"]); 
        $preiszielUSD = htmlentities($_REQUEST["preiszielUSD"]); 
        $link = htmlentities($_REQUEST["link"]);
        if ("/" == substr($link, -1)) {
            $link = substr($link, 0, -1);
        }
        $keepon = htmlentities($_REQUEST["keepon"]); 
        $keepoff = htmlentities($_REQUEST["keepoff"]); 
        

        //Addiert Datensatz
        $vitrinenCoinsArray[$key] = array(
            "Name" => $name,
            "kateg" => $kateg,
            "preiszielUSD" => $preiszielUSD,            
            "link" => $link,            
            "keepon" => $keepon,
            "keepoff" => $keepoff
        );
        //Umwandeln und Speichern
        $json_data = json_encode($vitrinenCoinsArray, JSON_PRETTY_PRINT);
        $isOK = file_put_contents($fileName, $json_data);
        
        header("Location: ./");
        
    }
        
}
    

if (isset($_REQUEST["deleteRow"])) {
    $delID = $_REQUEST["deleteRow"];

    $fileName = "../".$_SESSION['fileVitrinenListe'];
    $vitrinenListe = array();
    if (file_exists($fileName)) {
        $json_data = file_get_contents($fileName);
        $vitrinenListe = json_decode($json_data, true);
        
        unset($vitrinenListe[$delID]);
        
        $json_data = json_encode($vitrinenListe, JSON_PRETTY_PRINT);
        $isOK = file_put_contents($fileName, $json_data);  
        
        if ($isOK) {
            header("Location: ./");
        } else {
            echo "Error beim löschen aus der User-Datei.";
        }
            
    } else {
        echo "Error: Die User-Datei fehlt!  delID=".$delID. " , fileName=".$fileName;
    }
}

function getVitrinenListe() {
    $fileName = $_SESSION['fileVitrinenListe'];  
    if (file_exists($fileName)) {
        // echo "<br>file_exists($fileName)";
        $json_data = file_get_contents($fileName);
        $vitrinenListe = json_decode($json_data, true);
        
        $aktPreisDate = "";
        if (count($vitrinenListe) > 1) {
            foreach ($vitrinenListe as $key => $value) {
                $aktPreisDate = $value["aktPreisDate"];
                break;
            }
            $aktPreisDate = str_replace("_", " ", $aktPreisDate);
        }
                
        
        $out="
            <div id='jumpHere'>
        	  <form action='functions_vitrine.php' method='post' >
                <table class='table'>
                    <thead>
                        <tr>";
        if (isset($_SESSION["logged_in"])) {
        	$out.=         "<th><button name='addRow' value='addRow' class='button fa fa-plus' /></th>";
        }        
        $out.=             "<th>Ticker-<br>Symbol</th>
                            <th>Name,<br>Bezeichnung</th>
                            <th>Kategorie</th>
                            <th>Keep on</th>
                            <th>Preisziel<br> in USD</th>
                            <th>Akt. Preis <a href='callcoinprices.php'> <i class='fa fa-refresh'></i></a><br><span id='preisDat' style='font-size: 10px;'>".substr($aktPreisDate, 0, -3)." GMT</span></th>
                        <tr>
                    </thead>
                    <tbody id='mainTable'>
                 ";
                    if (count($vitrinenListe) < 1) {
                        $out .= "<tr>
                                    <td colspan='4'>Keine Daten vorhanden.<tr>
                                </tr>";
                    } else {
                        foreach ($vitrinenListe as $key => $value) {
                            $out .= "<tr>";
                            if (isset($_SESSION["logged_in"])) {
                            	$out.= "<td><button name='deleteRow' value='".$key."' class='button-in-liste fa fa-trash'/>
                            			<button name='editRow'   value='".$key."' class='button-in-liste fa fa-edit' /></td>";
                            }
                            $out.=     "<td>".$key."</td>";
                            $lnk = ""; 
                            if (isset($value["link"])) {
                                $lnk = $value["link"];
                                $out.=     "<td><a href='".$lnk."' target='_blank'>".$value["Name"]."</a></td>";                                
                            } else {
                                $out.=     "<td>".$value["Name"]."</td>";                                
                            }
                            $out.=     "<td>".$value["kateg"]."</td>
                                        <td>".$value["keepon"]."</td>
                                        <td>".$value["preiszielUSD"]."</td>
                                        <td>".substr($value["aktPreisUSD"], 0, -4)."</td>
                                    </tr>";
                        }
                    }
                    $out .= "
                    </tbody>
                </table>
              </form>
            </div>";
        return $out;
    }
}

?>