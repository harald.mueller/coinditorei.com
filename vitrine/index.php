<?php 
if (!isset($_SESSION)) { 
    session_start(); 
} 
include 'functions_vitrine.php'; 
include 'include.header.php';
?>
<p>Virtuelles Portfolio, Beobachtungsliste</p>
<h1>Vitrine</h1>
<?php 
if (isset($_SESSION["inputDetailFelder"]) AND ($_SESSION["inputDetailFelder"] == TRUE)) {
    include 'vitrinenDetailFelder.php';
}
echo getVitrinenListe(); //hier ist nicht nur die Liste drin, sondern auch alle CRUD-Funktionen  

include 'include.footer.php'; 
?>
