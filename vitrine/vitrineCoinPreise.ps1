﻿C:
cd \repo22\coinditorei.com\vitrine
$datum = Get-Date -Format yyyy-MM-dd_HH:mm
$vitrineCoinListOnlineFilename = "https://coinditorei.com/data/vitrinecoins.csv"
$vitrineCoinListFilename = "data/coinmarketcap/vitrinecoins.txt"
$vitrineCoinsPreiseFilename = "data/coinmarketcap/vitrinecoinpreise.txt"

Invoke-WebRequest -Uri $vitrineCoinListOnlineFilename -OutFile "../$vitrineCoinListFilename"
$vitrineCoinList = Get-Content "../$vitrineCoinListFilename"

$cName = ""
$linie = "vitrinecoin;$datum"
echo $linie
echo $linie > "../$vitrineCoinsPreiseFilename"
foreach ($cLink in  $vitrineCoinList) {
    if ($cLink.EndsWith("/")) {
        $cLink = $cLink.Remove($cLink.Length-1)
    }
    $cName = $cLink.Split("/")[4]
    $fileName = "..\data\coinmarketcap\$cName.txt"

    # Daten von CoinMarketCap holen
    Invoke-WebRequest -Uri $cLink -OutFile $fileName
    $inhalt = Get-Content $fileName 
    
    # Preis schnappen
    $len = $inhalt.Length
    $preisPos = $inhalt[$len-1].IndexOf("Price:")
    if ($preisPos -gt 0) {
        $preis = $inhalt[$len-1].substring($preisPos+19, 20)
    } else {
        $preisPos = $inhalt[$len-2].IndexOf("Price:")
        $preis = $inhalt[$len-2].substring($preisPos+19, 20)
    }
    $preis = $preis.Substring(0, $preis.IndexOf("</div") )
    
    $linie = "$cName;$preis"
    echo $linie
    $linie >> "../$vitrineCoinsPreiseFilename"
}


$client = New-Object System.Net.WebClient
$client.Credentials = New-Object System.Net.NetworkCredential("coinditoreicom", "Na,k0mmensR1")
$ftpFile =  "ftp://ftp.coinditorei.com/"+$vitrineCoinsPreiseFilename
$locFile = "c:/repo22/coinditorei.com/"+$vitrineCoinsPreiseFilename
# $client.UploadFile($ftpFile, $locFile)
