<?php
if (!isset($_SESSION)) {
    session_start();
}
$_SESSION['fileVitrinenListe'] = "../data/vitrine.json";
$fileName = "".$_SESSION['fileVitrinenListe'];
$vitrinenCoinsArray  = array();
if (file_exists($fileName)) {
    $json_data = file_get_contents($fileName);
    $vitrinenCoinsArray = json_decode($json_data, true);
}

foreach ($vitrinenCoinsArray as $key => $value) {
    $coinLinkArr = explode("/",$value["link"]);
    $coinLinkName = $coinLinkArr[count($coinLinkArr)-1];
    // echo '<br>-'.$key.' - https://crypto.com/price/'.$coinLinkName;
    $url = "https://crypto.com/price/".$coinLinkName;
    if ("/" == substr($url, -1)) {
        $url = substr($url, 0, -1);
    }
    $seitenInhalt = implode('', file($url));
    $needleS = '<span class="chakra-text css-13hqrwd">';
    $needleE = '</span>';
    $start = strpos($seitenInhalt, $needleS);
    $coinPreis = substr($seitenInhalt, $start + strlen($needleS), 40);
    $ende = strpos($coinPreis, $needleE);
    $coinPreis = substr($coinPreis, 0, $ende);
    
    // echo "<br>[" . $coinPreis . "] " . date("Y-m-d_H:i:s") . "<br>";
    
    $vitrinenCoinsArray[$key] = array(
        "Name" => $value["Name"],
        "kateg" => $value["kateg"],
        "preiszielUSD" => $value["preiszielUSD"],
        "link" => $value["link"],
        "keepon" => $value["keepon"],
        "keepoff" => $value["keepoff"],
        "aktPreisUSD" => $coinPreis,
        "aktPreisDate" => date("Y-m-d_H:i:s")
     );
}
$json_data = json_encode($vitrinenCoinsArray, JSON_PRETTY_PRINT);
$isOK = file_put_contents($fileName, $json_data);

//echo print_r($vitrinenCoinsArray, TRUE);

// $fileName = "bitcoin".date("Y-m-d-Hi").".txt";
// $isOK = file_put_contents($fileName, $_buffer);

header("location: ./");
?>