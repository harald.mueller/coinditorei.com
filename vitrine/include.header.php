<!DOCTYPE HTML>
<html>
	<head>
		<title>coinditorei, virtuelles portfolio</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700"> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
		<link rel="stylesheet" href="../css/main.css" />
        <meta name="description" content="Der Info-Shop über Kryptowährungen. Wir schauen den Coins ins Innere. Auf und in die Tortenböden, Füllungen, Crèmen, Couverturen usw.">
        <meta name="author" content="Bernhard Melmer, Harald G. Müller">
	</head>
	<body>
	
          <nav id="navbar">
           <!-- container Start-->
            <div class="container">
               
               <!--Row Start-->
               <div class="row">
                    <a href="../"><img src="../images/logo250g.png" class="logo" alt="logo"></a>
					<?php 
					if (isset($_SESSION["logged_in"])) {
						echo '<a href="../registrierung.php" style="background-color: #008040; color: white; padding: 0 0.3em 0 0.3em;" class="pull-right"><i class="fa fa-user"></i> '.$_SESSION["uKurzzeichen"].'</a>';
					} else {
						echo '<a href="../login.php" class="pull-right" style="padding: 0.3em;"><i class="fa fa-user"></i></a>';
					}
				    ?>
               </div>
               <!--Row Ended-->
                      
            </div>
           <!-- container Ended-->
          </nav>
	
	
		<div id="wrapper">

			<div class="Pagecontent"><!-- Pagecontent -->