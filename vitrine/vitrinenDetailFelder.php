<?php 
$key = "";
$name = "";
$kateg = "";
$preiszielUSD = "";
$link = "";
$keepon = date("Y-m-d");
$keepoff = "";

$disabled = "";

if (isset($_SESSION["editRow"])) {
    $editRow = $_SESSION["editRow"];
    unset($_SESSION["editRow"]);
    
    $fileName = $_SESSION['fileVitrinenListe'];
    if (file_exists($fileName)) {
        // echo "<br>file_exists($fileName)";
        $json_data = file_get_contents($fileName);
        $vitrinenListe = json_decode($json_data, true);
        if (count($vitrinenListe) > 0) {
            foreach ($vitrinenListe as $key => $value) {
                if ($editRow == $key) {
                    $name = $value["Name"];
                    $kateg = $value["kateg"];
                    $preiszielUSD = $value["preiszielUSD"];
                    $link = $value["link"];
                    $keepon = $value["keepon"];
                    $keepoff = $value["keepoff"];
                    break;
                }
            }
        }
    }
    $disabled = ' disabled="disabled" ';
}


?>
<div class="row">
    <section class="vitrinenDetailFelder"> 
    	<form action='functions_vitrine.php' method='post' >
    		<label for="key">Symbol</label>
			<input type="text" name="key" id="key" class="detailWerte" value="<?= $key ?>" <?= $disabled ?> required placeholder="Ticker-Symbol"/>
			<input type="hidden" name="hiddenKey" value="<?= $key ?>">
			
			<label for="name">Name</label>
			<input type="text" name="name" id="name" class="detailWerte" value="<?= $name ?>" required placeholder="Name"/>
			
			<label for="kateg">Kategorie</label>
			<input type="text" name="kateg" id="kateg" class="detailWerte" value="<?= $kateg ?>" placeholder="Kategorie"/>
			
			<label for="preiszielUSD">Preisziel USD</label>
			<input type="text" name="preiszielUSD" id="preiszielUSD" class="detailWerte" value="<?= $preiszielUSD ?>" placeholder="Preisziel USD"/>
						
			<label for="">Keep on</label>
			<input type="date" name="keepon" id="keepon" class="detailWerte" value="<?= $keepon ?>" min="2020-01-01" max="2039-12-31" placeholder="<?= $keepon ?>"/>

			<label for="">Keep off</label>
			<input type="date" name="keepoff" id="keepoff" class="detailWerte" value="<?= $keepoff ?>" min="2020-01-01" max="2039-12-31" placeholder="YYYY-MM-DD"/>

			<label for="">Link</label>
			<input type="url" name="link" id="link" class="detailWerte" value="<?= $link ?>" placeholder="https://....."/>
<br>
    		<ul class="actions pull-right">
    			<li><input type="submit" name="btn_cancel" value="Abbrechen"  /></li>
    			<li><input type="submit" name="btn_addSave" value="Speichern" class="special" /></li>
    		</ul>
    	</form>
    </section>
</div>
