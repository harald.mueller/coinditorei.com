        <!-- Grid column -->
        <div class="col-md-12 text-center">
          <div class="mb-5 flex-center">
            <!-- Facebook -->
            <a class="fb-ic">
              <i class="fa fa-facebook-official mr-4" aria-hidden="true"></i>
            </a>
            <!-- Twitter -->
            <a class="tw-ic">
              <i class="fa fa-twitter fa-lg mr-4" aria-hidden="true"> </i>
            </a>
            <!-- Google +-->
            <a class="gplus-ic">
              <i class="fa fa-pinterest fa-lg mr-4" aria-hidden="true"> </i>
            </a>
            <!--Linkedin -->
            <a class="li-ic">
              <i class="fa fa-youtube mr-4" aria-hidden="true"></i>
            </a>
            <!--Instagram-->
            <a class="ins-ic">
              <i class="fa fa-vimeo mr-4" aria-hidden="true"></i>
            </a>
            <!--Pinterest-->
            <a class="pin-ic">
              <i class="fa fa-google-plus fa-lg" aria-hidden="true"> </i>
            </a>
          </div>
        </div>
